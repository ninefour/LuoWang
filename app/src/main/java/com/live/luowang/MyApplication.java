package com.live.luowang;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.support.v4.media.MediaMetadataCompat;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.live.luowang.MusicServie.MusicService;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

/**
 * Created by Chen on 2016/4/11.
 */
public class MyApplication extends Application {
    private int duration=0;
    private static MyApplication instance;
    public static MyApplication getInstance(){
        return instance;
    }
    private RequestQueue queue;
    private MediaMetadataCompat metadataCompat;
    private static String mediaId;
    private static boolean isPlaying=false;
    private static boolean isMusicBar=false;

    public static String getMediaId() {
        return mediaId;
    }

    public static void setMediaId(String mediaId) {
        MyApplication.mediaId = mediaId;
    }

    public static boolean isMusicBar() {
        return isMusicBar;
    }

    public static void setIsMusicBar(boolean isMusicBar) {
        MyApplication.isMusicBar = isMusicBar;
    }

    public static boolean isPlaying() {
        return isPlaying;
    }

    public static void setIsPlaying(boolean isPlaying) {
        MyApplication.isPlaying = isPlaying;
    }

    public MediaMetadataCompat getMetadataCompat() {
        return metadataCompat;
    }

    public void setMetadataCompat(MediaMetadataCompat metadataCompat) {
        this.metadataCompat = metadataCompat;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance=this;
        queue= Volley.newRequestQueue(this);
        initImageLoader(getApplicationContext());
        starService();
    }

    private void starService(){
        Intent intent=new Intent(this,MusicService.class);
        startService(intent);
    }

    public RequestQueue getQueue(){
        return queue;
    }

    public void initImageLoader(Context context){
        ImageLoaderConfiguration.Builder config=new ImageLoaderConfiguration.Builder(context);
        DisplayImageOptions options=new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();
        config.defaultDisplayImageOptions(options);
        config.diskCacheSize(100 * 1024 * 1024);
        config.memoryCacheSize(20 * 1024 * 1024);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        config.writeDebugLogs();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        ImageLoader.getInstance().init(config.build());
    }
    public static DisplayImageOptions getOptions(){
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.default_bg)
//                 设置图片在下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.default_bg)
//                 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.default_bg)
//                 设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)
                // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true)
                // 设置下载的图片是否缓存在SD卡中
                .displayer(new FadeInBitmapDisplayer(300))// 图片加载好后渐入的动画时间
                .build();
        return options;
    }
    public static DisplayImageOptions getPlayingOptions(){
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.placeholder_disk_play_fm)
//                 设置图片在下载期间显示的图片
                .showImageForEmptyUri(R.mipmap.placeholder_disk_play_fm)
//                 设置图片Uri为空或是错误的时候显示的图片
                .showImageOnFail(R.mipmap.placeholder_disk_play_fm)
//                 设置图片加载/解码过程中错误时候显示的图片
                .cacheInMemory(true)
                // 设置下载的图片是否缓存在内存中
                .cacheOnDisk(true)
                // 设置下载的图片是否缓存在SD卡中
                .displayer(new FadeInBitmapDisplayer(0))// 图片加载好后渐入的动画时间
                .build();
        return options;
    }
    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }
}
