package com.live.luowang.MusicServie;

import android.support.v4.media.MediaMetadataCompat;

import com.live.luowang.JsonData.DiscoverDailyData;
import com.live.luowang.JsonData.ForumData;
import com.live.luowang.JsonData.MusictionInfoData;
import com.live.luowang.JsonData.NPreodicalData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/17.
 */
public class MusicProvider {
    public MusicProvider() {}
    public static List<MediaMetadataCompat> orderList =new ArrayList<>();
    public static List<NPreodicalData.TracksBean> tracksList=new ArrayList<>();
    public static List<MusictionInfoData.TrackBean> recommTracksList=new ArrayList<>();
    public static List<ForumData.DataBean.ItemsBean.PostAudioBean> postAudioBeanList=new ArrayList<>();
    public static List<DiscoverDailyData.DataBean.ItemsBean.PostAudioBean> DpostAudioBeanList=new ArrayList<>();

    public static void setDPostAudioBeanList(List<DiscoverDailyData.DataBean.ItemsBean.PostAudioBean> musicList) {
        DpostAudioBeanList = musicList;
    }
    public static void setPostAudioBeanList(List<ForumData.DataBean.ItemsBean.PostAudioBean> musicList) {
        postAudioBeanList = musicList;
    }
    public static void setRecommTracksList(List<MusictionInfoData.TrackBean> musicList) {
        recommTracksList = musicList;
    }
    public static void setMusic(List<NPreodicalData.TracksBean> musicList){
        tracksList=musicList;
    }
    public void updateMusic(){
        retrieveMedia(tracksList);
    }
    public void updateRecommMusic(){
        retrieveRecommMedia(recommTracksList);
    }
    public void updatePostaudioMusic(){
        retrievePostaudioMedia(postAudioBeanList);
    }
    public void updateDPostaudioMusic(){
        retrieveDPostaudioMedia(DpostAudioBeanList);
    }

    private MediaMetadataCompat buildFromObject(NPreodicalData.TracksBean tracksList) {
        String title = tracksList.getSongname();
        String album = tracksList.getSongalbum();
        String artist = tracksList.getSonger();
        String source = tracksList.getFilename320();
        String iconUrl = tracksList.getSongphoto();
        String id = tracksList.getResourcecode();
        String site=tracksList.getRemarks();
        String duration=tracksList.getTime();
        if(source == null || source.equals("")){
            source=tracksList.getFilename192();
            if (source == null || source.equals("")){
                source=tracksList.getFilename();
            }
        }
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_AUTHOR,duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE,site)
                .putString(MediaMetadataCompat.METADATA_KEY_DATE,source)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .build();
    }
    private MediaMetadataCompat buildFromRecommObject(MusictionInfoData.TrackBean tracksList) {
        String title = tracksList.getSongname();
        String artist = tracksList.getSonger();
        String source = tracksList.getFilename320();
        String iconUrl = tracksList.getSongphoto();
        String id = tracksList.getResourcecode();
        String site=tracksList.getResourcecode();
        String duration=tracksList.getTime();
        if(source.equals("")){
            source=tracksList.getFilename192();
            if (source.equals("")){
                source=tracksList.getFilename();
            }
        }
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_AUTHOR,duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE,site)
                .putString(MediaMetadataCompat.METADATA_KEY_DATE,source)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, "")
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .build();
    }
    private MediaMetadataCompat buildFromPostObject(ForumData.DataBean.ItemsBean.PostAudioBean tracksList) {
        String title = tracksList.getSong_name();
        String album = tracksList.getAlbum();
        String artist = tracksList.getArtist();
        String source = tracksList.getUrl();
        String iconUrl = tracksList.getCover();
        String id = tracksList.getSource_id();
        int site=tracksList.getSite();
        String duration="0";
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_AUTHOR,duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE,String.valueOf(site))
                .putString(MediaMetadataCompat.METADATA_KEY_DATE,source)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .build();
    }
    private MediaMetadataCompat buildFromDPostObject(DiscoverDailyData.DataBean.ItemsBean.PostAudioBean tracksList) {
        String title = tracksList.getSong_name();
        String album = tracksList.getAlbum();
        String artist = tracksList.getArtist();
        String source = tracksList.getUrl();
        String iconUrl = tracksList.getCover();
        String id = tracksList.getSource_id();
        int site=tracksList.getSite();
        String duration="0";
        return new MediaMetadataCompat.Builder()
                .putString(MediaMetadataCompat.METADATA_KEY_AUTHOR,duration)
                .putString(MediaMetadataCompat.METADATA_KEY_GENRE,String.valueOf(site))
                .putString(MediaMetadataCompat.METADATA_KEY_DATE,source)
                .putString(MediaMetadataCompat.METADATA_KEY_MEDIA_ID, id)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM, album)
                .putString(MediaMetadataCompat.METADATA_KEY_ARTIST, artist)
                .putString(MediaMetadataCompat.METADATA_KEY_ALBUM_ART_URI, iconUrl)
                .putString(MediaMetadataCompat.METADATA_KEY_TITLE, title)
                .build();
    }
    private void retrieveMedia(List<NPreodicalData.TracksBean> list) {
        orderList=new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            MediaMetadataCompat item = buildFromObject(list.get(j));
            orderList.add(item);
        }
    }
    private void retrieveRecommMedia(List<MusictionInfoData.TrackBean> list) {
        orderList=new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            MediaMetadataCompat item = buildFromRecommObject(list.get(j));
            orderList.add(item);
        }
    }
    private void retrievePostaudioMedia(List<ForumData.DataBean.ItemsBean.PostAudioBean> list) {
        orderList=new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            MediaMetadataCompat item = buildFromPostObject(list.get(j));
            orderList.add(item);
        }
    }
    private void retrieveDPostaudioMedia(List<DiscoverDailyData.DataBean.ItemsBean.PostAudioBean> list) {
        orderList=new ArrayList<>();
        for (int j = 0; j < list.size(); j++) {
            MediaMetadataCompat item = buildFromDPostObject(list.get(j));
            orderList.add(item);
        }
    }
    public static List<MediaMetadataCompat> getOrderList(){
        return orderList;
    }
}
