package com.live.luowang.Activity.InnerActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.Adapter.RecommdationViewPagerAdapter;
import com.live.luowang.Fragment.Recommendation.RecommendationFragment;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.JsonData.MusitionData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2016/4/10.
 */
public class RecommendationActivity extends AppCompatActivity implements View.OnClickListener{
    private InnerActivityPrensenter prensenter;
    List<MusitionData.ListBean> musitionDataList=new ArrayList<>();
    private RecommdationViewPagerAdapter adapter;
    @Bind(R.id.PreArrow)
    ImageView preArrow;
    @Bind(R.id.ARecommendationViewPager)
    ViewPager viewPager;
    private MusitionData musitionData;
    private int page=1;
    private int position;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recommendation_layout);
        ButterKnife.bind(this);
        position=getIntent().getIntExtra("position",0);
        page=getIntent().getIntExtra("page",1);
        musitionDataList= (List<MusitionData.ListBean>) getIntent().getExtras().get("data");
        prensenter=new InnerActivityPrensenterImpl();
        adapter=new RecommdationViewPagerAdapter(getSupportFragmentManager());
        RecommendationFragment fragment;
        for (int i=0;i<musitionDataList.size();i++){
            fragment=RecommendationFragment.getInstance(musitionDataList.get(i));
            adapter.addFragment(fragment);
        }
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
//        getData(page);
        setListener();
    }

    private void setListener(){
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                if(position==adapter.getCount()-1){
                    getData(++page);
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        preArrow.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {prensenter.preArrows(this);}

    @Override
    protected void onDestroy() {
        super.onDestroy();
        adapter.clearList();
    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            RecommendationFragment fragment;
            for(int i=0;i<musitionData.getList().size();i++){
                fragment=RecommendationFragment.getInstance(musitionData.getList().get(i));
                adapter.addFragment(fragment);
            }
            adapter.notifyDataSetChanged();
        }
    };

    private void getData(int page){
        String url= HttpUrl.MUSITION+String.valueOf(page);
        StringRequest request=new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                response=response.substring(7);
                response=response.substring(0,response.length()-1);
                musitionData = gson.fromJson(response,MusitionData.class);
                handler.sendEmptyMessage(1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }
}
