package com.live.luowang.Activity.TabHostActivity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.live.luowang.MusicServie.MusicPlayingActivity;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

public class TabHostActivity extends AppCompatActivity
        implements RadioGroup.OnCheckedChangeListener,View.OnClickListener, MusicPlayingActivity.Callback {
    private FragmentManager manager;
    private RadioGroup radioGroup;
    private TabHostPrensenter tabhostPrensenter;
    private DrawerLayout drawerLayout;
    private ImageView menuSetting;
    private ImageView loading;
    private RelativeLayout musicBar;
    private TextView title, artist;
    private SeekBar seekBar;
    private ImageView play,pause;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_host_layout);
        loading= (ImageView) findViewById(R.id.Loading);
        musicBar= (RelativeLayout) findViewById(R.id.MusicBar);
        title= (TextView) findViewById(R.id.MusicBarTitle);
        artist= (TextView) findViewById(R.id.MusicBarArtist);
        seekBar= (SeekBar) findViewById(R.id.MusicBarSeek);
        play= (ImageView) findViewById(R.id.MusicBarPlay);
        pause= (ImageView) findViewById(R.id.MusicBarPause);
        AnimationDrawable animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        drawerLayout = (DrawerLayout) findViewById(R.id.TabHostDrawerLayout);
        menuSetting = (ImageView) findViewById(R.id.MenuSetting);
        menuSetting.setOnClickListener(this);
        musicBar.setOnClickListener(this);
        radioGroup = (RadioGroup) findViewById(R.id.TabHostRadioGroup);
        radioGroup.check(R.id.TabHostDiscorverBt);
        radioGroup.setOnCheckedChangeListener(this);
        manager = getSupportFragmentManager();
        tabhostPrensenter = new TabHostPrensenterImpl();
        MusicPlayingActivity.setCallback(this);
        initView();
        registerReceiver(receiver, new IntentFilter("DailyChange"));
        registerReceiver(showMusicBar, new IntentFilter("musicBar"));
        registerReceiver(updateSeekBar, new IntentFilter("current"));
        registerReceiver(musicPause, new IntentFilter("musicPause"));
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                play.setVisibility(View.GONE);
//                pause.setVisibility(View.VISIBLE);
                Intent intent=new Intent();
                intent.setAction("musicBarAction");
                intent.putExtra("action",0);
                sendBroadcast(intent);
            }
        });
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                pause.setVisibility(View.GONE);
//                play.setVisibility(View.VISIBLE);
                Intent intent=new Intent();
                intent.setAction("musicBarAction");
                intent.putExtra("action",1);
                sendBroadcast(intent);
            }
        });
    }

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            findViewById(R.id.TabHostForumBt).callOnClick();
            tabhostPrensenter.changeToDaily(manager,radioGroup);
        }};

    BroadcastReceiver showMusicBar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
//            findViewById(R.id.TabHostForumBt).callOnClick();
            if (MyApplication.isPlaying()){
                musicBar.setVisibility(View.VISIBLE);
            }
        }};
    BroadcastReceiver updateSeekBar = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            seekBar.setProgress(intent.getIntExtra("current",0));
            play.setVisibility(View.GONE);
            pause.setVisibility(View.VISIBLE);
        }
    };
    BroadcastReceiver musicPause =new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            pause.setVisibility(View.GONE);
            play.setVisibility(View.VISIBLE);
        }
    };

    public void toogle() {
        tabhostPrensenter.toggle(drawerLayout);
    }

    private void initView() {
        tabhostPrensenter.setFragment(manager);
    }

    @Override
    public void onClick(View v) {
        tabhostPrensenter.onClick(v, this);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        tabhostPrensenter.onCheckChange(checkedId, manager);}

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        tabhostPrensenter.exitBy2Click(this, keyCode);
        return false;
    }

    @Override
    protected void onDestroy() {
        unregisterReceiver(receiver);
        unregisterReceiver(showMusicBar);
        unregisterReceiver(updateSeekBar);
        unregisterReceiver(musicPause);
        super.onDestroy();}

    @Override
    public void setMetaData(String title, String artist, int duration) {
        this.title.setText(title);
        this.artist.setText(artist);
        this.seekBar.setMax(duration);
    }

}
