package com.live.luowang.Activity.InnerActivity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.live.luowang.Adapter.PeriodicalPlayViewPagerAdapter;
import com.live.luowang.Fragment.PeriodicalFragment.PeriodicalPlayFragment;
import com.live.luowang.JsonData.NPreodicalData;
import com.live.luowang.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class PeriodicalPlayActivity extends AppCompatActivity {
    @Bind(R.id.PreArrow)
    ImageView preArrow;
    @Bind(R.id.PPlayViewPager)
    ViewPager viewPager;
    private List<NPreodicalData> nPreodicalDataList;
    private PeriodicalPlayViewPagerAdapter adapter;
    private int position;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {//4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_periodical_play_layout);
        ButterKnife.bind(this);
        viewPager.setOffscreenPageLimit(5);
        nPreodicalDataList= (List<NPreodicalData>) getIntent().getExtras().getSerializable("data");
        position=getIntent().getIntExtra("position",0);
        adapter=new PeriodicalPlayViewPagerAdapter(getSupportFragmentManager());
        PeriodicalPlayFragment fragment;
        for (int i=0;i<nPreodicalDataList.size();i++){
            fragment=PeriodicalPlayFragment.getInstance(nPreodicalDataList.get(i));
            adapter.addFragment(fragment);
        }
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(position);
        preArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
