package com.live.luowang.Activity.InnerActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.live.luowang.Adapter.FormHotInfoAdapter;
import com.live.luowang.Adapter.FormHotInfoDAdapter;
import com.live.luowang.JsonData.DiscoverDailyData;
import com.live.luowang.JsonData.ForumData;
import com.live.luowang.MusicServie.MusicPlayingActivity;
import com.live.luowang.MusicServie.MusicProvider;
import com.live.luowang.MusicServie.MusicService;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FormHotInfoActivity extends AppCompatActivity {

    @Bind(R.id.PPlayListview)
    ListView listview;
    @Bind(R.id.PreArrow)
    ImageView preArrow;
    private TextView content;
    private TextView contentless;
    private ImageView topicImage;
    private TextView moreContent;
    private TextView userName;
    private ImageView avatar;
    private ForumData.DataBean.ItemsBean itemsBean;
    private DiscoverDailyData.DataBean.ItemsBean DitemsBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forum_hot_layout);
        ButterKnife.bind(this);
        if (getIntent().getBooleanExtra("discover",false)){
            DitemsBean= (DiscoverDailyData.DataBean.ItemsBean) getIntent().getExtras().get("data");
            listview.addHeaderView(getDHeader());
            listview.setAdapter(new FormHotInfoDAdapter(DitemsBean.getPost_audio(),this));
            listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if (position!=0) {
                        MusicProvider.setDPostAudioBeanList(DitemsBean.getPost_audio());
                        Intent intent = new Intent(FormHotInfoActivity.this, MusicService.class);
                        intent.putExtra("click", true);
                        intent.putExtra("type", 3);
                        intent.putExtra("position", position - 1);
                        startService(intent);
                        Intent intent1 = new Intent(FormHotInfoActivity.this, MusicPlayingActivity.class);
                        startActivity(intent1);
                    }
                }
            });
        }else {
            itemsBean = (ForumData.DataBean.ItemsBean) getIntent().getExtras().get("data");
            listview.addHeaderView(getHeader());
            if (itemsBean.getPost_audio() != null) {
                listview.setAdapter(new FormHotInfoAdapter(itemsBean.getPost_audio(), this));
                listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        if (position!=0) {
                            MusicProvider.setPostAudioBeanList(itemsBean.getPost_audio());
                            Intent intent = new Intent(FormHotInfoActivity.this, MusicService.class);
                            intent.putExtra("click", true);
                            intent.putExtra("type", 2);
                            intent.putExtra("position", position - 1);
                            startService(intent);
                            Intent intent1 = new Intent(FormHotInfoActivity.this, MusicPlayingActivity.class);
                            startActivity(intent1);
                        }
                    }
                });
            } else {
                listview.setAdapter(new FormHotInfoAdapter(new ArrayList<>(), this));
            }
        }
    }

    private View getHeader() {
        View view = LayoutInflater.from(this).inflate(R.layout.header_form_hot_info_layout, null);
        content = (TextView) view.findViewById(R.id.PPlayContent);
        contentless = (TextView) view.findViewById(R.id.PPlayContentLess);
        moreContent = (TextView) view.findViewById(R.id.PPlayMoreContent);
        topicImage = (ImageView) view.findViewById(R.id.NPeriodicalTopicImage);
        avatar = (ImageView) view.findViewById(R.id.PPlayAvatar);
        userName = (TextView) view.findViewById(R.id.PPlayUserName);
        userName.setText(itemsBean.getUser_name());
        content.setText(itemsBean.getContent());
        contentless.setText(itemsBean.getContent());
        if (itemsBean.getPost_img()==null){
            topicImage.setVisibility(View.GONE);
        }else {
            ImageLoader.getInstance().displayImage(itemsBean.getPost_img().get(0), topicImage, MyApplication.getOptions());
        }
        ImageLoader.getInstance().displayImage(itemsBean.getUser_avatar(), avatar);
        moreContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentless.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                moreContent.setVisibility(View.GONE);
            }
        });
        return view;
    }

    private View getDHeader() {
        View view = LayoutInflater.from(this).inflate(R.layout.header_form_hot_info_layout, null);
        content = (TextView) view.findViewById(R.id.PPlayContent);
        contentless = (TextView) view.findViewById(R.id.PPlayContentLess);
        moreContent = (TextView) view.findViewById(R.id.PPlayMoreContent);
        topicImage = (ImageView) view.findViewById(R.id.NPeriodicalTopicImage);
        avatar = (ImageView) view.findViewById(R.id.PPlayAvatar);
        userName = (TextView) view.findViewById(R.id.PPlayUserName);
        userName.setText(DitemsBean.getUser_name());
        content.setText(DitemsBean.getContent());
        contentless.setText(DitemsBean.getContent());
        if (DitemsBean.getPost_img()==null){
            topicImage.setVisibility(View.GONE);
        }else {
            ImageLoader.getInstance().displayImage(DitemsBean.getPost_img().get(0), topicImage, MyApplication.getOptions());
        }
        ImageLoader.getInstance().displayImage(DitemsBean.getUser_avatar(), avatar);
        moreContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentless.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                moreContent.setVisibility(View.GONE);
            }
        });
        return view;
    }

    @OnClick(R.id.PreArrow)
    public void onClick() {
        finish();
    }
}
