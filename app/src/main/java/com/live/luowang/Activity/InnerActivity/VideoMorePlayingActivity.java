package com.live.luowang.Activity.InnerActivity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoMorePlayingActivity extends AppCompatActivity {

    @Bind(R.id.PVideoBackgroundImg)
    ImageView backgroundImg;
    @Bind(R.id.PVideoImg)
    ImageView img;
    @Bind(R.id.PVideoTitle)
    TextView title;
    @Bind(R.id.VideoType)
    TextView type;
    @Bind(R.id.VideoDuration)
    TextView duration;
    @Bind(R.id.PreArrow)
    ImageView preArrow;
    @Bind(R.id.VideoContent)
    TextView content;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_playing_layout);
        ButterKnife.bind(this);
        Intent intent=getIntent();
        title.setText(intent.getStringExtra("title"));
        type.setText("#" + intent.getStringExtra("category"));
        duration.setText(DateUtils.formatElapsedTime(intent.getIntExtra("duration",0)));
        content.setText(intent.getStringExtra("description"));
        ImageLoader.getInstance().displayImage(intent.getStringExtra("detail"), img);
        ImageLoader.getInstance().displayImage(intent.getStringExtra("blurred"), backgroundImg);
        final String url=intent.getStringExtra("playUrl");
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoMorePlayingActivity.this, VideoPlayFullScreenActivity.class);
                intent.putExtra("url", url);
                startActivity(intent);
            }
        });
        preArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
