package com.live.luowang.Activity.InnerActivity;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import com.live.luowang.Adapter.ArticleActivityListViewAdapter;
import com.live.luowang.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ArticleActivity extends AppCompatActivity {
    @Bind(R.id.AArticleListView)
    ListView listView;
    @Bind(R.id.AArticleSwipeRefresh)
    SwipeRefreshLayout swipeRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_layout);
        ButterKnife.bind(this);
        listView.setAdapter(new ArticleActivityListViewAdapter(this));
    }
}
