package com.live.luowang.Activity.InnerActivity;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.live.luowang.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoPlayFullScreenActivity extends AppCompatActivity implements AudioManager.OnAudioFocusChangeListener{
    @Bind(R.id.FullVideo)
    VideoView video;
    private MediaController controller;
    private String url;
    private ProgressBar progressBar;
    private String TAG = "MainActivity";
    private GestureDetector mGestureDetector;
    private AudioManager audioManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);//隐藏标题
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);//设置全屏
        setContentView(R.layout.activity_video_play_full_screen_layout);
        ButterKnife.bind(this);
        audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        progressBar= (ProgressBar) findViewById(R.id.progressBar);
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
            }
        });
        video.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                video.setVisibility(View.GONE);
                giveUpAudioFocus();
                finish();
            }
        });
        controller=new MediaController(this);
        video.setMediaController(controller);
        controller.setMediaPlayer(video);
        url=getIntent().getStringExtra("url");
        video.setVideoURI(Uri.parse(url));
        video.requestFocus();
        tryToGetAudioFocus();
        video.start();
    }

    private boolean tryToGetAudioFocus() {
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);
        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            return true;
        }
        return false;
    }
    private void giveUpAudioFocus() {
        audioManager.abandonAudioFocus(this);
    }
    @Override
    public void onAudioFocusChange(int focusChange) {

    }
    @Override
    protected void onStart() {
        super.onStart();

    }

    public void setBrightness(float brightness) {
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        // if (lp.screenBrightness <= 0.1) {
        // return;
        // }
        lp.screenBrightness = lp.screenBrightness + brightness / 255.0f;
        if (lp.screenBrightness > 1) {
            lp.screenBrightness = 1;
//            vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
//            long[] pattern = { 10, 200 }; // OFF/ON/OFF/ON...
//            vibrator.vibrate(pattern, -1);
        } else if (lp.screenBrightness < 0.2) {
            lp.screenBrightness = (float) 0.2;
//            vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
//            long[] pattern = { 10, 200 }; // OFF/ON/OFF/ON...
//            vibrator.vibrate(pattern, -1);
        }
        Log.e(TAG, "lp.screenBrightness= " + lp.screenBrightness);
        getWindow().setAttributes(lp);
    }
    @Override
    protected void onResume() {
        mGestureDetector = new GestureDetector(
                new GestureDetector.OnGestureListener() {
                    public boolean onSingleTapUp(MotionEvent e) {
                        return false;
                    }
                    public boolean onDown(MotionEvent e) {
                        return false;
                    }
                    public void onLongPress(MotionEvent e) {
                    }
                    public boolean onFling(MotionEvent e1, MotionEvent e2,
                                           float velocityX, float velocityY) {
                        return true;
                    }
                    public boolean onScroll(MotionEvent e1, MotionEvent e2,
                                            float distanceX, float distanceY) {
                        final double FLING_MIN_DISTANCE = 0.5;
                        final double FLING_MIN_VELOCITY = 0.5;
                        if (e1.getY() - e2.getY() > FLING_MIN_DISTANCE
                                && Math.abs(distanceY) > FLING_MIN_VELOCITY) {
                            Log.e(TAG, "up");
                            setBrightness(40);
                        }
                        if (e1.getY() - e2.getY() < FLING_MIN_DISTANCE
                                && Math.abs(distanceY) > FLING_MIN_VELOCITY) {
                            Log.e(TAG, "down");
                            setBrightness(-40);
                        }
                        return true;
                    }
                    public void onShowPress(MotionEvent e) {
                        // TODO Auto-generated method stub
                    }
                });
        super.onResume();
    }
    public boolean onTouchEvent(MotionEvent event) {
        boolean result = mGestureDetector.onTouchEvent(event);
        if (!result) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                // getVideoInfosfromPath(filePath);
            }
            result = super.onTouchEvent(event);
        }
        return result;
    }
    @Override
    protected void onStop() {
//        if (null != vibrator) {
//            vibrator.cancel();
//        }
        super.onStop();
    }
}
