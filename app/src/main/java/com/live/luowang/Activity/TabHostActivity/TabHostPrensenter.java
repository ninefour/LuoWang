package com.live.luowang.Activity.TabHostActivity;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.RadioGroup;

/**
 * Created by Chen on 2016/4/5.
 */
public interface TabHostPrensenter {

    void onCheckChange(int checkId,FragmentManager manager);

    void exitBy2Click(Context context,int keyCode);

    void setFragment(FragmentManager manager);

    void toggle(DrawerLayout drawerLayout);

    void onClick(View view,Context context);

    void changeToDaily(FragmentManager manager, RadioGroup radioGroup);
}
