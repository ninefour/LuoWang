package com.live.luowang.Activity.InnerActivity;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.Adapter.VideoMoreListViewAdapter;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.Function.RefreshLayout;
import com.live.luowang.JsonData.VideoMoreData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/5.
 */
public class MoreVideoActivity extends AppCompatActivity implements RefreshLayout.OnRefreshListener,RefreshLayout.OnLoadListener{
    private VideoMoreData videoData;
    private ListView actListView;
    private VideoMoreListViewAdapter adapter;
    private ImageView loading;
    private AnimationDrawable animationDrawable;
    private RefreshLayout refreshLayout;
    private TextView title;
    private ImageView preArrow;
    private int page=1;
    private int id;
    private List<VideoMoreData.ItemListBean> itemListBeen=new ArrayList<>();
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_layout);
        actListView= (ListView) findViewById(R.id.AArticleListView);
        loading= (ImageView) findViewById(R.id.Loading);
        refreshLayout= (RefreshLayout) findViewById(R.id.AArticleSwipeRefresh);
        title= (TextView) findViewById(R.id.VideoMoreTitle);
        preArrow= (ImageView) findViewById(R.id.PreArrow);
        refreshLayout.setColorSchemeResources(R.color.titleText);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadListener(this);
        animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        actListView= (ListView) findViewById(R.id.AArticleListView);
        id=getIntent().getIntExtra("id",0);
        title.setText(getIntent().getStringExtra("title").substring(1));
        preArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        getData(id);
    }

    @Override
    public void onRefresh() {
        page=1;
        getData(id);
    }

    @Override
    public void onLoad() {
        page++;
        getLoadData();
    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            animationDrawable.stop();
            loading.setVisibility(View.GONE);
            switch (msg.what){
                case 1:
                    itemListBeen=videoData.getItemList();
                    refreshLayout.setRefreshing(false);
                    adapter=new VideoMoreListViewAdapter(MoreVideoActivity.this,itemListBeen);
                    actListView.setAdapter(adapter);
                    break;
                case 2:
                    refreshLayout.setLoading(false);
                    adapter.addList(videoData.getItemList());
                    adapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    private void getData(int id){
        StringRequest request=new StringRequest(0, HttpUrl.VIDEOMORE+String.valueOf(id), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                videoData=gson.fromJson(response,VideoMoreData.class);
                handler.sendEmptyMessage(1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }
    private void getLoadData(){
        StringRequest request=new StringRequest(0, videoData.getNextPageUrl(), new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                videoData=gson.fromJson(response,VideoMoreData.class);
                handler.sendEmptyMessage(2);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }
}
