package com.live.luowang.Activity.InnerActivity;

import android.app.Activity;
import android.content.Context;

/**
 * Created by Chen on 2016/4/12.
 */
public class InnerActivityPrensenterImpl implements InnerActivityPrensenter {
    @Override
    public void preArrows(Context context) {
        ((Activity)context).finish();
    }

    @Override
    public void periodicalListener() {

    }

    @Override
    public void loadNextPage() {

    }

    @Override
    public void loadPrePage() {

    }
}
