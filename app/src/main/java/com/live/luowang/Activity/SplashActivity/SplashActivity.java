package com.live.luowang.Activity.SplashActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.ImageView;

import com.live.luowang.Activity.TabHostActivity.TabHostActivity;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.Function.JsonRequest;
import com.live.luowang.JsonData.SplashData;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 */
public class SplashActivity extends AppCompatActivity implements JsonRequest.CallBack {
    @Bind(R.id.SplashImg)
    ImageView splashImg;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_layout);
        ButterKnife.bind(this);
        JsonRequest.setCallBack(this);
        JsonRequest.getRequest(HttpUrl.SPLASH);
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                handler.sendEmptyMessage(1);
            }
        }, 5000);
    }
    private void jumping(){
        Intent intent = new Intent(SplashActivity.this,
                TabHostActivity.class);
        startActivity(intent);
        finish();
    }
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(final Message msg) {
            super.handleMessage(msg);
            jumping();
        }
    };

    @Override
    public void call(Object data, String type) {
        ImageLoader.getInstance().displayImage(
                ((SplashData)data).getData().getStartup().getImg(),
                splashImg,
                new DisplayImageOptions.Builder()
                        .displayer(new FadeInBitmapDisplayer(4000))
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .build());
    }
}
