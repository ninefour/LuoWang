package com.live.luowang.Activity.InnerActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.JsonData.VideoData;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;

public class VideoPlayingActivity extends AppCompatActivity {

    @Bind(R.id.PVideoBackgroundImg)
    ImageView backgroundImg;
    @Bind(R.id.PVideoImg)
    ImageView img;
    @Bind(R.id.PVideoTitle)
    TextView title;
    @Bind(R.id.VideoType)
    TextView type;
    @Bind(R.id.VideoDuration)
    TextView duration;
    @Bind(R.id.PreArrow)
    ImageView preArrow;
    @Bind(R.id.VideoContent)
    TextView content;

    private VideoData.IssueListBean.ItemListBean.DataBean dataBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){//4.4 全透明状态栏
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE);
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }
        setContentView(R.layout.activity_video_playing_layout);
        ButterKnife.bind(this);
        dataBean = (VideoData.IssueListBean.ItemListBean.DataBean) getIntent().getExtras().get("data");
        title.setText(dataBean.getTitle());
        type.setText("#" + dataBean.getCategory());
        duration.setText(DateUtils.formatElapsedTime(dataBean.getDuration()));
        content.setText(dataBean.getDescription());
        ImageLoader.getInstance().displayImage(dataBean.getCover().getDetail(), img);
        ImageLoader.getInstance().displayImage(dataBean.getCover().getBlurred(), backgroundImg);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(VideoPlayingActivity.this, VideoPlayFullScreenActivity.class);
                intent.putExtra("url", dataBean.getPlayUrl());
                startActivity(intent);
            }
        });
        preArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

}
