package com.live.luowang.Activity.TabHostActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.live.luowang.Fragment.Discover.DiscoverFragment;
import com.live.luowang.Fragment.Event.EventFragment;
import com.live.luowang.Fragment.Forum.ForumFragment;
import com.live.luowang.MusicServie.MusicPlayingActivity;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Chen on 2016/4/5.
 */
public class TabHostPrensenterImpl implements TabHostPrensenter {
    static DiscoverFragment discoverFragment=new DiscoverFragment();
    static ForumFragment forumFragment=new ForumFragment();
    static EventFragment eventFragment=new EventFragment();
    private static boolean isExit=false;

    @Override
    public void onClick(View view,Context context) {
        switch (view.getId()){
            case R.id.MenuMine:
                break;
            case R.id.MenuSetting:
                break;
            case R.id.MenuMySite:
                break;
            case R.id.MenuMessage:
                break;
            case R.id.MusicBar:
                MyApplication.setIsMusicBar(true);
                Intent intent=new Intent(context, MusicPlayingActivity.class);
                context.startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onCheckChange(int checkedId,FragmentManager manager) {
        switch (checkedId) {
            case R.id.TabHostDiscorverBt:
                manager.beginTransaction().hide(forumFragment).commit();
                manager.beginTransaction().hide(eventFragment).commit();
                if(!discoverFragment.isAdded()){
                    manager.beginTransaction().add(R.id.TabHostFrameLayout,discoverFragment).commit();
                    manager.beginTransaction().show(discoverFragment).commit();
                }else {
                    manager.beginTransaction().show(discoverFragment).commit();
                }
                break;
            case R.id.TabHostForumBt:
                Log.i("jereh","=====================");
                manager.beginTransaction().hide(eventFragment).commit();
                manager.beginTransaction().hide(discoverFragment).commit();
                if(!forumFragment.isAdded()){
                    Log.i("jereh","isadd=====================");
                    manager.beginTransaction().add(R.id.TabHostFrameLayout,forumFragment).commit();
                    manager.beginTransaction().show(forumFragment).commit();
                }else {
                    manager.beginTransaction().show(forumFragment).commit();
                }
                break;
            case R.id.TabHostEventBt:
                manager.beginTransaction().hide(discoverFragment).commit();
                manager.beginTransaction().hide(forumFragment).commit();
                if(!eventFragment.isAdded()){
                    manager.beginTransaction().add(R.id.TabHostFrameLayout,eventFragment).commit();
                    manager.beginTransaction().show(eventFragment).commit();
                }else {
                    manager.beginTransaction().show(eventFragment).commit();
                }
                break;
        }
    }

    @Override
    public void setFragment(FragmentManager manager) {
        manager.beginTransaction().add(R.id.TabHostFrameLayout,discoverFragment).commit();
    }

    @Override
    public void exitBy2Click(Context context,int keyCode) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            if (MyApplication.getInstance().isPlaying()) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                context.startActivity(intent);
            } else {
                Timer timer;
                if (isExit == false) {
                    isExit = true;
                    Toast.makeText(context, "再按一次退出落网", Toast.LENGTH_SHORT).show();
                    timer = new Timer();
                    timer.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            isExit = false;
                        }
                    }, 2000);
                } else {
                    ((Activity) context).finish();
                }
            }
        }
    }

    @Override
    public void toggle(DrawerLayout drawerLayout) {
        drawerLayout.openDrawer(GravityCompat.START);
    }

    @Override
    public void changeToDaily(FragmentManager manager, final RadioGroup radioGroup) {
        onCheckChange(R.id.TabHostForumBt,manager);
        final Handler handler=new Handler();
        new Thread(){
            @Override
            public void run() {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        radioGroup.check(R.id.TabHostForumBt);
                        forumFragment.onPageSelected(1);
                    }
                });
            }
        }.start();
    }
}
