package com.live.luowang.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

import com.live.luowang.JsonData.ForumData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/6.
 */
public class ForumFollowListViewAdapter extends BaseAdapter {
    private Context context;
    private List<ForumData.DataBean.RecommendBean> recommendBeanList=new ArrayList<>();

    public ForumFollowListViewAdapter(Context context,List recommendBeanList) {
        this.context = context;
        this.recommendBeanList=recommendBeanList;
    }

    @Override
    public int getCount() {
        return recommendBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return recommendBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        if(recommendBeanList.get(position).getPosts()==null){
            return 0;
        }else {
            return 1;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ForumData.DataBean.RecommendBean recommendBean=recommendBeanList.get(position);
        ViewHolder holder=null;
        switch (getItemViewType(position)) {
            case 1:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = LayoutInflater.from(context).inflate(R.layout.item_forum_follow_listview_layout, null);
                    holder.headImg = (ImageView) convertView.findViewById(R.id.FollowHeadimg);
                    holder.userName = (TextView) convertView.findViewById(R.id.FollowUserName);
                    holder.followCount = (TextView) convertView.findViewById(R.id.FollowCount);
                    holder.followFollower = (TextView) convertView.findViewById(R.id.FollowFollower);
                    holder.toFollow= (CheckBox) convertView.findViewById(R.id.FollowToFollow);
                    holder.tableRow = (TableRow) convertView.findViewById(R.id.FollowTableRow);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                holder.tableRow.removeAllViews();
                holder.toFollow.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    }
                });
                for (int i = 0; i < recommendBean.getPosts().size(); i++) {
                    holder.itemView = LayoutInflater.from(context).inflate(R.layout.item_forum_follow_item_layout, null);
                    holder.topicImg = (ImageView) holder.itemView.findViewById(R.id.FollowTopicImg);
                    holder.topicTitle = (TextView) holder.itemView.findViewById(R.id.FollowTopicTitle);
                    holder.topicContent = (TextView) holder.itemView.findViewById(R.id.FollowTopicContent);
                    holder.topicTitle.setText(recommendBean.getPosts().get(i).getPost_audio().get(0).getSong_name());
                    holder.topicContent.setText(recommendBean.getPosts().get(i).getPost_audio().get(0).getArtist());
                    ImageLoader.getInstance().displayImage(recommendBean.getPosts().get(i).getPost_audio().get(0).getCover(),holder.topicImg, MyApplication.getOptions());
                    holder.tableRow.addView(holder.itemView);
                }
                for (int i = 0; i < 3-recommendBean.getPosts().size(); i++) {
                    holder.itemView = LayoutInflater.from(context).inflate(R.layout.item_forum_follow_item_empty_layout, null);
                    holder.tableRow.addView(holder.itemView);
                }
                break;
            case 0:
                if (convertView == null) {
                    holder = new ViewHolder();
                    convertView = LayoutInflater.from(context).inflate(R.layout.item_forum_follow_listview_noimg_layout, null);
                    holder.headImg = (ImageView) convertView.findViewById(R.id.FollowHeadimg);
                    holder.userName = (TextView) convertView.findViewById(R.id.FollowUserName);
                    holder.followCount = (TextView) convertView.findViewById(R.id.FollowCount);
                    holder.followFollower = (TextView) convertView.findViewById(R.id.FollowFollower);
                    convertView.setTag(holder);
                } else {
                    holder = (ViewHolder) convertView.getTag();
                }
                break;
            default:
                break;
            }
        holder.userName.setText(recommendBean.getUser_name());
        holder.followCount.setText(String.valueOf(recommendBean.getPost_count()));
        holder.followFollower.setText(String.valueOf(recommendBean.getFans_count()));
        ImageLoader.getInstance().displayImage(recommendBean.getAvatar(),holder.headImg);
        return convertView;
    }

    private class ViewHolder{
        private ImageView headImg;
        private TextView userName;
        private TextView followCount;
        private TextView followFollower;
        private View itemView;
        private ImageView topicImg;
        private TextView topicTitle;
        private TextView topicContent;
        private TableRow tableRow;
        private CheckBox toFollow;
    }
}
