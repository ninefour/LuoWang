package com.live.luowang.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.JsonData.DiscoverHeaderData;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/5.
 */
public class ArticleListViewAdapter extends BaseAdapter {
    private List<DiscoverHeaderData.DataBean.EssaysBean> essaysBeanList=new ArrayList<>();
    private Context context;

    public ArticleListViewAdapter(Context context,List essaysBeanList) {
        this.context = context;
        this.essaysBeanList=essaysBeanList;
    }

    @Override
    public int getCount() {
        return essaysBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return essaysBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        DiscoverHeaderData.DataBean.EssaysBean essaysBean=essaysBeanList.get(position);
        ViewHolder holder;
        if (convertView==null){
            holder=new ViewHolder();
            convertView= LayoutInflater.from(context).inflate(R.layout.item_article_listview_layout,null);
            holder.topicImg= (ImageView) convertView.findViewById(R.id.ArticleTopicImage);
            holder.content= (TextView) convertView.findViewById(R.id.ArticleSubscribe);
            holder.title= (TextView) convertView.findViewById(R.id.ArticleTitle);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.content.setText(essaysBean.getSubscribe());
        holder.title.setText(essaysBean.getTitle());
        ImageLoader.getInstance().displayImage(essaysBean.getImageurl(),holder.topicImg);
        return convertView;
    }

    private class ViewHolder{
        private ImageView topicImg;
        private TextView title;
        private TextView content;
    }
}
