package com.live.luowang.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.JsonData.NPreodicalData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/19.
 */
public class PeriodicalPlayListViewAdapter extends BaseAdapter {
    private List<NPreodicalData.TracksBean> tracksBeanList = new ArrayList<>();
    private Context context;

    public PeriodicalPlayListViewAdapter(List<NPreodicalData.TracksBean> tracksBeanList, Context context) {
        this.tracksBeanList = tracksBeanList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return tracksBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return tracksBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        NPreodicalData.TracksBean trackBean=tracksBeanList.get(position);
        ViewHolder holder;
        if (convertView==null) {
            holder=new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.item_pplistview_layout, null);
            holder.album= (ImageView) convertView.findViewById(R.id.PPListViewAlbum);
            holder.artist= (TextView) convertView.findViewById(R.id.PPListViewArtist);
            holder.name= (TextView) convertView.findViewById(R.id.PPListViewName);
            holder.num= (TextView) convertView.findViewById(R.id.PPListViewNum);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        if(position<9) {
            holder.num.setText("0" + String.valueOf(position+1));
        }else {
            holder.num.setText(String.valueOf(position+1));
        }
        holder.artist.setText(trackBean.getSonger());
        holder.name.setText(trackBean.getSongname());
        ImageLoader.getInstance().displayImage(trackBean.getThumbnail_url(),holder.album, MyApplication.getOptions());
        return convertView;
    }
    private class ViewHolder{
        private TextView num;
        private TextView name;
        private TextView artist;
        private ImageView album;
    }
}
