package com.live.luowang.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.live.luowang.Activity.WebViewActivity;
import com.live.luowang.JsonData.DiscoversData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/5.
 */
public class BannerViewPagerAdapter extends PagerAdapter {
    private List<DiscoversData.AdBean> ads4BeanList=new ArrayList<>();
    private Context context;
    public BannerViewPagerAdapter(Context context,List ads4BeanList) {
        this.context = context;
        this.ads4BeanList=ads4BeanList;
    }
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        ImageView imageView=new ImageView(context);
        ImageLoader.getInstance().displayImage(ads4BeanList.get(position%ads4BeanList.size()).getImage(),imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, WebViewActivity.class);
                intent.putExtra("url",ads4BeanList.get(position%ads4BeanList.size()).getLink());
                context.startActivity(intent);
            }
        });
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}
