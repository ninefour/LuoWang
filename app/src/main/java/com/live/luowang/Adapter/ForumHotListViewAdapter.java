package com.live.luowang.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.JsonData.ForumData;
import com.live.luowang.MusicServie.MusicPlayingActivity;
import com.live.luowang.MusicServie.MusicProvider;
import com.live.luowang.MusicServie.MusicService;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/6.
 */
public class ForumHotListViewAdapter extends BaseAdapter {
    private List<ForumData.DataBean.ItemsBean> itemsBeen=new ArrayList<>();
    private Context context;

    public ForumHotListViewAdapter(Context context,List itemsBeen) {
        this.context = context;
        this.itemsBeen=itemsBeen;
    }

    @Override
    public int getItemViewType(int position) {
        if(itemsBeen.get(position).getPost_img()==null && itemsBeen.get(position).getPost_audio()==null){
            return 3;
        }else if (itemsBeen.get(position).getPost_img()==null){
            return 0;
        }else if(itemsBeen.get(position).getPost_audio()==null){
            return 1;
        }else {
            return 2;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getCount() {
        return itemsBeen.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsBeen.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ForumData.DataBean.ItemsBean itemsBean=itemsBeen.get(position);
        ViewHolder holder;
        switch (getItemViewType(position)){
            case 0:{
                if (convertView==null){
                    holder=new ViewHolder();
                    convertView= LayoutInflater.from(context).inflate(R.layout.item_forum_hot_listview_noimg_layout,null);
                    holder.headImg= (ImageView) convertView.findViewById(R.id.HotHeadimg);
                    holder.songsPoster= (ImageView) convertView.findViewById(R.id.HotSongsPoster);
                    holder.content= (TextView) convertView.findViewById(R.id.HotContent);
                    holder.songsArtisit= (TextView) convertView.findViewById(R.id.HotSongsArtisit);
                    holder.songsName= (TextView) convertView.findViewById(R.id.HotSongsName);
                    holder.songsCount= (TextView) convertView.findViewById(R.id.HotSongsCount);
                    holder.userName= (TextView) convertView.findViewById(R.id.HotUserName);
                    convertView.setTag(holder);
                }else {
                    holder= (ViewHolder) convertView.getTag();
                }
                holder.content.setText(itemsBean.getContent());
                holder.userName.setText(itemsBean.getUser_name());
                holder.songsArtisit.setText(itemsBean.getPost_audio().get(0).getArtist());
                holder.songsName.setText(itemsBean.getPost_audio().get(0).getSong_name());
                if (itemsBean.getPost_audio().size() == 1) {
                    holder.songsCount.setVisibility(View.GONE);
                } else {
                    holder.songsCount.setText(String.valueOf(itemsBean.getPost_audio().size()) + "首");
                }
                ImageLoader.getInstance().displayImage(itemsBean.getPost_audio().get(0).getCover(),holder.songsPoster);
                ImageLoader.getInstance().displayImage(itemsBean.getAvatar(),holder.headImg);
                holder.songsPoster.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MusicProvider.setPostAudioBeanList(itemsBean.getPost_audio());
                        Intent intent=new Intent(context, MusicService.class);
                        intent.putExtra("click",true);
                        intent.putExtra("type",2);
                        context.startService(intent);
                        Intent intent1=new Intent(context, MusicPlayingActivity.class);
                        context.startActivity(intent1);
                    }
                });
                break;
            }
            case 1:{
                if (convertView==null){
                    holder=new ViewHolder();
                    convertView= LayoutInflater.from(context).inflate(R.layout.item_forum_hot_listview_nosongs_layout,null);
                    holder.topicImg= (ImageView) convertView.findViewById(R.id.HotImg);
                    ViewGroup.LayoutParams params=holder.topicImg.getLayoutParams();
                    params.height=context.getResources().getDisplayMetrics().widthPixels;
                    holder.topicImg.setLayoutParams(params);
                    holder.headImg= (ImageView) convertView.findViewById(R.id.HotHeadimg);
                    holder.content= (TextView) convertView.findViewById(R.id.HotContent);
                    holder.userName= (TextView) convertView.findViewById(R.id.HotUserName);
                    convertView.setTag(holder);
                }else {
                    holder= (ViewHolder) convertView.getTag();
                }
                holder.content.setText(itemsBean.getContent());
                holder.userName.setText(itemsBean.getUser_name());
                ImageLoader.getInstance().displayImage(itemsBean.getAvatar(),holder.headImg);
                ImageLoader.getInstance().displayImage(itemsBean.getPost_img().get(0), holder.topicImg, MyApplication.getOptions());
                break;
            }
            case 2:{
                if (convertView==null){
                    holder=new ViewHolder();
                    convertView= LayoutInflater.from(context).inflate(R.layout.item_forum_hot_listview_layout,null);
                    holder.topicImg= (ImageView) convertView.findViewById(R.id.HotImg);
                    ViewGroup.LayoutParams params=holder.topicImg.getLayoutParams();
                    params.height=context.getResources().getDisplayMetrics().widthPixels;
                    holder.topicImg.setLayoutParams(params);
                    holder.headImg= (ImageView) convertView.findViewById(R.id.HotHeadimg);
                    holder.songsPoster= (ImageView) convertView.findViewById(R.id.HotSongsPoster);
                    holder.content= (TextView) convertView.findViewById(R.id.HotContent);
                    holder.songsArtisit= (TextView) convertView.findViewById(R.id.HotSongsArtisit);
                    holder.songsName= (TextView) convertView.findViewById(R.id.HotSongsName);
                    holder.songsCount= (TextView) convertView.findViewById(R.id.HotSongsCount);
                    holder.userName= (TextView) convertView.findViewById(R.id.HotUserName);
                    convertView.setTag(holder);
                }else {
                    holder= (ViewHolder) convertView.getTag();
                }
                holder.content.setText(itemsBean.getContent());
                holder.userName.setText(itemsBean.getUser_name());
                holder.songsArtisit.setText(itemsBean.getPost_audio().get(0).getArtist());
                holder.songsName.setText(itemsBean.getPost_audio().get(0).getSong_name());
                if (itemsBean.getPost_audio().size() == 1) {
                    holder.songsCount.setVisibility(View.GONE);
                } else {
                    holder.songsCount.setText(String.valueOf(itemsBean.getPost_audio().size()) + "首");
                }
                ImageLoader.getInstance().displayImage(itemsBean.getPost_audio().get(0).getCover(),holder.songsPoster);
                ImageLoader.getInstance().displayImage(itemsBean.getAvatar(),holder.headImg);
                ImageLoader.getInstance().displayImage(itemsBean.getPost_img().get(0), holder.topicImg,MyApplication.getOptions());
                holder.songsPoster.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MusicProvider.setPostAudioBeanList(itemsBean.getPost_audio());
                        Intent intent=new Intent(context, MusicService.class);
                        intent.putExtra("click",true);
                        intent.putExtra("type",2);
                        context.startService(intent);
                        Intent intent1=new Intent(context, MusicPlayingActivity.class);
                        context.startActivity(intent1);
                    }
                });
                break;
            }
            case 3:{
                if (convertView==null){
                    holder=new ViewHolder();
                    convertView= LayoutInflater.from(context).inflate(R.layout.item_forum_hot_listview_nothing_layout,null);
                    holder.headImg= (ImageView) convertView.findViewById(R.id.HotHeadimg);
                    holder.content= (TextView) convertView.findViewById(R.id.HotContent);
                    holder.userName= (TextView) convertView.findViewById(R.id.HotUserName);
                    convertView.setTag(holder);
                }else {
                    holder= (ViewHolder) convertView.getTag();
                }
                holder.content.setText(itemsBean.getContent());
                holder.userName.setText(itemsBean.getUser_name());ImageLoader.getInstance().displayImage(itemsBean.getAvatar(),holder.headImg);
                break;
            }
        }

        return convertView;
    }

    private class ViewHolder{
        private ImageView headImg;
        private ImageView songsPoster;
        private TextView userName;
        private TextView content;
        private TextView songsName;
        private TextView songsArtisit;
        private TextView songsCount;
        private ImageView topicImg;
    }
}
