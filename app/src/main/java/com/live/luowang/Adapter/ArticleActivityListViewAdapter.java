package com.live.luowang.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.R;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2016/4/11.
 */
public class ArticleActivityListViewAdapter extends BaseAdapter {
    private Context context;

    public ArticleActivityListViewAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_articleactivity_listview_layout, null);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.AArticleItemTopicImage)
        ImageView topicImage;
        @Bind(R.id.AArticleItemTitle)
        TextView title;
        @Bind(R.id.AArticleItemAuthor)
        TextView author;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
