package com.live.luowang.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/12.
 */
public class RecommdationViewPagerAdapter extends FragmentPagerAdapter {
    List<Fragment> fragmentList=new ArrayList<>();

    public RecommdationViewPagerAdapter(FragmentManager fm) {super(fm);}

    public void addFragment(Fragment fragment){fragmentList.add(fragment);}

    public void clearList(){fragmentList.clear();}

    @Override
    public Fragment getItem(int position) {return fragmentList.get(position);}

    @Override
    public int getCount() {return fragmentList.size();}
}
