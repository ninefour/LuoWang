package com.live.luowang.Adapter;

import android.content.Context;
import android.support.v4.media.MediaMetadataCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.live.luowang.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Chen on 2016/4/27.
 */
public class MusicListViewAdapter extends BaseAdapter {
    private List<MediaMetadataCompat> metadataList = new ArrayList<>();
    private Context context;
    public MusicListViewAdapter(List<MediaMetadataCompat> metadataList, Context context) {
        this.metadataList = metadataList;
        this.context = context;
    }

    @Override
    public int getCount() {
        return metadataList.size();
    }

    @Override
    public Object getItem(int position) {
        return metadataList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MediaMetadataCompat metadata=metadataList.get(position);
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_musiclist_layout, null);
            holder=new ViewHolder(convertView);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.name.setText(metadata.getText(MediaMetadataCompat.METADATA_KEY_TITLE));
        holder.artist.setText(" - "+metadata.getText(MediaMetadataCompat.METADATA_KEY_ARTIST));
//        if (MyApplication.getMediaId().equals(metadata.getDescription().getMediaId())){
//            holder.name.setTextColor(context.getResources().getColor(R.color.titleText));
//            holder.artist.setTextColor(context.getResources().getColor(R.color.titleText));
//        }else {
//            holder.name.setTextColor(context.getResources().getColor(R.color.contentText));
//            holder.artist.setTextColor(context.getResources().getColor(R.color.contentText));
//        }
        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.MusicListName)
        TextView name;
        @Bind(R.id.MusicListArtist)
        TextView artist;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
