package com.live.luowang.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.live.luowang.JsonData.NPreodicalData;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/19.
 */
public class PPlayGridViewAdapter extends RecyclerView.Adapter<PPlayGridViewAdapter.ViewHolder> {
    private List<NPreodicalData.ListenBean> listenBeanList=new ArrayList<>();
    private Context context;

    public PPlayGridViewAdapter(List<NPreodicalData.ListenBean> listenBeanList, Context context) {
        this.listenBeanList = listenBeanList;
        this.context = context;
    }

    @Override
    public PPlayGridViewAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        ViewHolder holder=new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pplay_gridview_layout,viewGroup,false));
        return holder;
    }

    @Override
    public void onBindViewHolder(PPlayGridViewAdapter.ViewHolder viewHolder, int i) {
        ImageLoader.getInstance().displayImage(listenBeanList.get(i).getPimg(),viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return listenBeanList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageView;
        public ViewHolder(View v) {
            super(v);
            imageView= (ImageView) v.findViewById(R.id.PPlayGridViewImg);
        }
    }
}
