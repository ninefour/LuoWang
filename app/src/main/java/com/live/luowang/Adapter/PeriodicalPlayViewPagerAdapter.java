package com.live.luowang.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.live.luowang.Fragment.PeriodicalFragment.PeriodicalPlayFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/20.
 */
public class PeriodicalPlayViewPagerAdapter extends FragmentPagerAdapter {
    private List<PeriodicalPlayFragment> fragmentList=new ArrayList<>();
    public PeriodicalPlayViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void addFragment(PeriodicalPlayFragment fragment){
        fragmentList.add(fragment);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
