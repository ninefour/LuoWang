package com.live.luowang.Adapter;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.Activity.InnerActivity.VideoMorePlayingActivity;
import com.live.luowang.JsonData.VideoMoreData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/6.
 */
public class VideoMoreListViewAdapter extends BaseAdapter {
    private List<VideoMoreData.ItemListBean> itemsBeanList = new ArrayList<>();
    private Context context;

    public VideoMoreListViewAdapter(Context context, List itemsBeanList) {
        this.context = context;
        this.itemsBeanList=itemsBeanList;
    }
    public void addList(List<VideoMoreData.ItemListBean> itemsBeanList){
        this.itemsBeanList.addAll(itemsBeanList);
    }
    @Override
    public int getCount() {
        return itemsBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final VideoMoreData.ItemListBean.DataBean dataBean=itemsBeanList.get(position).getData();
        ViewHolder holder;
        if(convertView==null) {
            holder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(R.layout.include_article_layout, null);
            holder.duration = (TextView) convertView.findViewById(R.id.VideoDuration);
            holder.type = (TextView) convertView.findViewById(R.id.VideoType);
            holder.title = (TextView) convertView.findViewById(R.id.VideoTitle);
            holder.img = (ImageView) convertView.findViewById(R.id.VideoImg);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.title.setText(dataBean.getTitle());
        holder.type.setText("#" + dataBean.getCategory());
        holder.duration.setText(DateUtils.formatElapsedTime(dataBean.getDuration()));
        ImageLoader.getInstance().displayImage(dataBean.getCover().getDetail(), holder.img, MyApplication.getOptions());
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, VideoMorePlayingActivity.class);
                intent.putExtra("title",dataBean.getTitle());
                intent.putExtra("description",dataBean.getDescription());
                intent.putExtra("category",dataBean.getCategory());
                intent.putExtra("detail",dataBean.getCover().getDetail());
                intent.putExtra("blurred",dataBean.getCover().getBlurred());
                intent.putExtra("playUrl",dataBean.getPlayUrl());
                intent.putExtra("duration",dataBean.getDuration());
                context.startActivity(intent);
            }
        });
        return convertView;
    }

    static class ViewHolder {
        private ImageView img;
        private TextView title;
        private TextView type;
        private TextView duration;
    }
}
