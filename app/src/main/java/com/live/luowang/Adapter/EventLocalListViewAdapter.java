package com.live.luowang.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.Activity.InnerActivity.MoreVideoActivity;
import com.live.luowang.JsonData.VideoDiscoverData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/6.
 */
public class EventLocalListViewAdapter extends BaseAdapter {
    private List<VideoDiscoverData.ItemListBean.DataBean> dataBeanList=new ArrayList<>();
    private Context context;

    public EventLocalListViewAdapter(Context context,List<VideoDiscoverData.ItemListBean> itemsBeanList) {
        for (int i = 4; i < itemsBeanList.size(); i++) {
            dataBeanList.add(itemsBeanList.get(i).getData());
        }
        this.context = context;
    }

    @Override
    public int getCount() {
        return dataBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final VideoDiscoverData.ItemListBean.DataBean itemsBean=dataBeanList.get(position);
        final ViewHolder holder;
        if (convertView==null){
            holder=new ViewHolder();
            convertView= LayoutInflater.from(context).inflate(R.layout.item_event_videomore_gridview_layout,null);
            holder.image= (ImageView) convertView.findViewById(R.id.VideoMoreImg);
            holder.type= (TextView) convertView.findViewById(R.id.VideoMoreType);
            holder.shadow= (ImageView) convertView.findViewById(R.id.VideoMoreShadow);
            convertView.setTag(holder);
        }else {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.type.setText(itemsBean.getTitle());
        ImageLoader.getInstance().displayImage(itemsBean.getImage(),holder.image, MyApplication.getOptions());
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, MoreVideoActivity.class);
                intent.putExtra("id",itemsBean.getId());
                intent.putExtra("title",itemsBean.getTitle());
                context.startActivity(intent);
            }
        });
        holder.shadow.requestFocus();
        holder.shadow.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
            @Override
            public void onViewAttachedToWindow(View v) {
                AnimationSet animationSet = new AnimationSet(true);
                AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
                alphaAnimation.setDuration(1000);
                animationSet.addAnimation(alphaAnimation);
                holder.shadow.startAnimation(animationSet);
                holder.type.startAnimation(alphaAnimation);
                holder.shadow.setVisibility(View.VISIBLE);
                holder.type.setVisibility(View.VISIBLE);
            }
            @Override
            public void onViewDetachedFromWindow(View v) {
            }
        });
        return convertView;
    }
    private class ViewHolder{
        private ImageView image;
        private TextView type;
        private ImageView shadow;
    }
}
