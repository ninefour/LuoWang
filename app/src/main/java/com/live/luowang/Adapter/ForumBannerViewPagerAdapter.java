package com.live.luowang.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.live.luowang.JsonData.ForumBannerData;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/5.
 */
public class ForumBannerViewPagerAdapter extends PagerAdapter {
    private List<ForumBannerData.DataBean.ItemsBean> itemsBeen=new ArrayList<>();
    private Context context;
    public ForumBannerViewPagerAdapter(Context context, List itemsBeen) {
        this.context = context;
        this.itemsBeen=itemsBeen;
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imageView=new ImageView(context);
        ImageLoader.getInstance().displayImage(itemsBeen.get(position%itemsBeen.size()).getImage(),imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view==object;
    }
}

