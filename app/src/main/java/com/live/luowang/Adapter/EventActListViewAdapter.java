package com.live.luowang.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.AnimationSet;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.live.luowang.Activity.InnerActivity.VideoPlayingActivity;
import com.live.luowang.JsonData.VideoData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/6.
 */
public class EventActListViewAdapter extends BaseAdapter {
    private List<VideoData.IssueListBean.ItemListBean> itemsBeanList = new ArrayList<>();
    private Context context;

    public EventActListViewAdapter(Context context) {
        this.context = context;
    }
    public void addListItem(List<VideoData.IssueListBean> issueListBeen) {
        for (int i = 0; i < issueListBeen.get(0).getCount(); i++) {
            itemsBeanList.add(issueListBeen.get(0).getItemList().get(i));
        }
        for (int i = 0; i < issueListBeen.get(1).getCount(); i++) {
            itemsBeanList.add(issueListBeen.get(1).getItemList().get(i));
        }

    }

    @Override
    public int getCount() {
        return itemsBeanList.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsBeanList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        String type=itemsBeanList.get(position).getType();
        switch (type){
            case "banner1":
                return 0;
            case "imageHeader":
                return 1;
            case "video":
                return 2;
            case "textHeader":
                return 3;
            default:
                return 4;
        }
    }

    @Override
    public int getViewTypeCount() {
        return super.getViewTypeCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final VideoData.IssueListBean.ItemListBean itemListBean = itemsBeanList.get(position);
        final ViewHolder holder;
        switch (getItemViewType(position)){
            case 0: {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.include_article_layout, null);
                holder.img= (ImageView) convertView.findViewById(R.id.VideoImg);
                ImageLoader.getInstance().displayImage(itemListBean.getData().getImage(), holder.img);
                break;
            }
            case 1: {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.video_img_header_layout, null);
                holder.imgHeader= (ImageView) convertView.findViewById(R.id.VideoImgheader);
                ImageLoader.getInstance().displayImage(itemListBean.getData().getImage(), holder.imgHeader);
                break;
            }
            case 2: {
                holder = new ViewHolder();
                convertView = LayoutInflater.from(context).inflate(R.layout.include_article_layout, null);
                holder.duration= (TextView) convertView.findViewById(R.id.VideoDuration);
                holder.type= (TextView) convertView.findViewById(R.id.VideoType);
                holder.title= (TextView) convertView.findViewById(R.id.VideoTitle);
                holder.img= (ImageView) convertView.findViewById(R.id.VideoImg);
                holder.shadow= (ImageView) convertView.findViewById(R.id.VideoShadow);
                holder.title.setText(itemListBean.getData().getTitle());
                holder.type.setText("#" + itemListBean.getData().getCategory());
                holder.duration.setText(DateUtils.formatElapsedTime(itemListBean.getData().getDuration()));
                ImageLoader.getInstance().displayImage(itemListBean.getData().getCover().getDetail(), holder.img, MyApplication.getOptions());
                holder.img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent=new Intent(context, VideoPlayingActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putParcelable("data",itemListBean.getData());
                        intent.putExtras(bundle);
                        context.startActivity(intent);
                    }
                });
                holder.shadow.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener() {
                    @Override
                    public void onViewAttachedToWindow(View v) {
                        AnimationSet animationSet = new AnimationSet(true);
                        AlphaAnimation alphaAnimation = new AlphaAnimation(0, 1);
                        alphaAnimation.setDuration(1000);
                        animationSet.addAnimation(alphaAnimation);
                        holder.shadow.startAnimation(animationSet);
                        holder.title.startAnimation(animationSet);
                        holder.duration.startAnimation(animationSet);
                        holder.type.startAnimation(animationSet);
                    }

                    @Override
                    public void onViewDetachedFromWindow(View v) {

                    }
                });
                break;
            }
            case 3: {
                holder=new ViewHolder();
                convertView=LayoutInflater.from(context).inflate(R.layout.video_text_header_layout,null);
                holder.text= (TextView) convertView.findViewById(R.id.VideoText);
                holder.text.setText(itemListBean.getData().getText());
                Typeface typeface=Typeface.createFromAsset(context.getAssets(),"fonts/Lobster-1.4.otf");
                holder.text.setTypeface(typeface);
                break;
            }
            case 4:
                convertView=new View(context);
                break;
        }
        return convertView;
    }

    static class ViewHolder {
        private ImageView img;
        private TextView title;
        private TextView type;
        private TextView duration;
        private TextView text;
        private ImageView imgHeader;
        private ImageView shadow;
    }
}
