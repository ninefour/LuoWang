package com.live.luowang.Function;

import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.live.luowang.Activity.InnerActivity.NewestPeriodicalActivity;
import com.live.luowang.Activity.InnerActivity.NewsRecommendationActivity;
import com.live.luowang.Adapter.BannerViewPagerAdapter;
import com.live.luowang.Adapter.ForumBannerViewPagerAdapter;
import com.live.luowang.JsonData.DiscoversData;
import com.live.luowang.JsonData.ForumBannerData;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Chen on 2016/4/1.
 */
public class FunctionClass {
    //DiscoverFragment获取HeaderView
    public synchronized static View getDiscoverHead(final Context context, final DiscoversData data, final SwipeRefreshLayout layout) {
        RelativeLayout periodicalMore, dailyMore;
        LinearLayout recommMore;
        View view = LayoutInflater.from(context).inflate(R.layout.header_discover_layout, null);
        //radiogroup动态添加radiobutton
        final RadioGroup discoverRadioGroup = (RadioGroup) view.findViewById(R.id.DiscoverRadiogroup);
        for(int i=0;i<data.getAd().size();i++){
            RadioButton radioButton=new RadioButton(context);
            radioButton.setBackgroundResource(R.drawable.radiobutton_style);
            radioButton.setButtonDrawable(null);
            RadioGroup.LayoutParams params= new RadioGroup.LayoutParams(18,18);
            params.setMargins(5,0,5,0);
            radioButton.setId(i);
            discoverRadioGroup.addView(radioButton,params);
        }
        discoverRadioGroup.check(0);
        //原创推荐
        ImageView rPoster= (ImageView) view.findViewById(R.id.RecommendPoster);
        TextView rName= (TextView) view.findViewById(R.id.RecommendName);
        TextView rArtist= (TextView) view.findViewById(R.id.RecommendArtist);
        rName.setText(data.getMusician().getTrackname());
        rArtist.setText(data.getMusician().getDescription());
        ImageLoader.getInstance().displayImage(data.getMusician().getImage(),rPoster);
        //顶部Banner
        final ViewPager discoverViewPager = (ViewPager) view.findViewById(R.id.DiscoverViewPager);
        discoverViewPager.setAdapter(new BannerViewPagerAdapter(context, data.getAd()));
        discoverViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                position=position%data.getAd().size();
                discoverRadioGroup.check(position);
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        discoverViewPager.setCurrentItem(300);
        discoverViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_MOVE:
                        layout.setEnabled(false);
                        break;
                    case MotionEvent.ACTION_UP:
                    case MotionEvent.ACTION_CANCEL:
                        layout.setEnabled(true);
                        break;
                }
                return false;
            }
        });
        //自动循环
        final Handler handler=new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                discoverViewPager.setCurrentItem(discoverViewPager.getCurrentItem()+1);
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(1);
                }
            }
        }).start();
        //音乐期刊
        DiscoversData.AlbumBean volsBean=data.getAlbum();
        ImageView PeriodicalTopicImage= (ImageView) view.findViewById(R.id.PeriodicalTopicImage);
        TextView PeriodicalVols= (TextView) view.findViewById(R.id.PeriodicalVols);
        TextView PeriodicalTitle= (TextView) view.findViewById(R.id.PeriodicalTitle);
        TextView PeriodicalFav= (TextView) view.findViewById(R.id.PeriodicalFav);
        LinearLayout linearLayout= (LinearLayout) view.findViewById(R.id.PeriodicalLinear);
        ImageLoader.getInstance().displayImage(volsBean.getMphoto(),PeriodicalTopicImage);
        PeriodicalVols.setText(String.valueOf(volsBean.getMnum()));
        PeriodicalTitle.setText(String.valueOf(volsBean.getMname()));
        PeriodicalFav.setText(String.valueOf(volsBean.getPlay_count()));
        linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(context, NewestPeriodicalActivity.class);
                context.startActivity(intent);
            }
        });
        //跳转
        periodicalMore = (RelativeLayout) view.findViewById(R.id.PeriodicalMore);
        recommMore= (LinearLayout) view.findViewById(R.id.RecommMore);
        dailyMore = (RelativeLayout) view.findViewById(R.id.DailyMore);
        periodicalMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewestPeriodicalActivity.class);
                context.startActivity(intent);
            }
        });
        recommMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, NewsRecommendationActivity.class);
                context.startActivity(intent);
            }
        });
        dailyMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.sendBroadcast(new Intent().setAction("DailyChange"));
            }
        });
        return view;
    }

    //ForumHotFragment获取HeaderView
    public static View getForumHotHead(Context context, final ForumBannerData data) {
        View view = LayoutInflater.from(context).inflate(R.layout.header_forum_hot_layout, null);
        final RadioGroup forumHotRadioGroup = (RadioGroup) view.findViewById(R.id.ForumHotRadiogroup);
        if(data.getData().getItems()==null){
            return new View(context);
        }
        for(int i=0;i<data.getData().getItems().size();i++){
            RadioButton radioButton=new RadioButton(context);
            radioButton.setBackgroundResource(R.drawable.radiobutton_style);
            radioButton.setButtonDrawable(null);
            RadioGroup.LayoutParams params= new RadioGroup.LayoutParams(18,18);
            params.setMargins(5,0,5,0);
            radioButton.setId(i);
            forumHotRadioGroup.addView(radioButton,params);
        }
        forumHotRadioGroup.check(0);
        //ForumHot Banner
        final ViewPager forumHotViewPager = (ViewPager) view.findViewById(R.id.ForumHotViewPager);
        forumHotViewPager.setOffscreenPageLimit(0);
        forumHotViewPager.setAdapter(new ForumBannerViewPagerAdapter(context, data.getData().getItems()));
        forumHotViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                position=position%data.getData().getItems().size();
                forumHotRadioGroup.check(position);}
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
        //自动循环
        forumHotViewPager.setCurrentItem(300);
        final Handler handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what){
                    case 2:
                        forumHotViewPager.setCurrentItem(forumHotViewPager.getCurrentItem() + 1);
                }
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    handler.sendEmptyMessage(2);
                }
            }
        }).start();
        return view;
    }


    //ForumFollowFragment获取HeaderView
    public static View getForumFollowHead(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.header_forum_follow_layout, null);
        return view;
    }

    //EventListView获取FootView
    public static View getEventFoot(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.footer_event_listview_layout, null);
        return view;
    }

    //调整listview高度
    public static void fixListViewHeight(ListView listView) {
        // 如果没有设置数据适配器，则ListView没有子项，返回。
        ListAdapter listAdapter = listView.getAdapter();
        int totalHeight = 0;
        if (listAdapter == null) {
            return;
        }
        for (int i = 0, len = listAdapter.getCount(); i < len; i++) {
            View listViewItem = listAdapter.getView(i, null, listView);
            // 计算子项View 的宽高
            listViewItem.measure(0, 0);
            // 计算所有子项的高度和
            totalHeight += listViewItem.getMeasuredHeight();
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // listView.getDividerHeight()获取子项间分隔符的高度
        // params.height设置ListView完全显示需要的高度
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

}
