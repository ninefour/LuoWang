package com.live.luowang.Function;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.JsonData.DiscoverDailyData;
import com.live.luowang.JsonData.DiscoversData;
import com.live.luowang.JsonData.SplashData;
import com.live.luowang.MyApplication;

/**
 * Created by Chen on 2016/4/13.
 */
public class JsonRequest {
    private static CallBack callBack;
    public interface CallBack{
        void call(Object data,String type);
    }
    public static void setCallBack(CallBack call){
        callBack=call;
    }
    public static void getRequest(final String url){
        final StringRequest request=new StringRequest(0, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                switch (url){
                    case HttpUrl.MAINDAILY:{
                        DiscoverDailyData discoverDailyData =gson.fromJson(response,DiscoverDailyData.class);
                        callBack.call(discoverDailyData,"MAINDAILY");
                        break;
                    }
                    case HttpUrl.SPLASH:{
                        SplashData splashData=gson.fromJson(response,SplashData.class);
                        callBack.call(splashData,"SPLASH");
                        break;
                    }
                    case HttpUrl.DISCOVERHEADERS:{
                        DiscoversData discoversData=gson.fromJson(response,DiscoversData.class);
                        callBack.call(discoversData,"DISCOVERHEADER");
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }
}
