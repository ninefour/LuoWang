package com.live.luowang.Fragment.Event;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.live.luowang.Adapter.FragmentViewPagerAdapter;
import com.live.luowang.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/5.
 */
public class EventFragment extends Fragment{
    private RadioGroup radioGroup;
    private ViewPager viewPager;
    private FragmentViewPagerAdapter adapter;
    private List<Fragment> fragmentList=new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_event_layout,null);
        radioGroup= (RadioGroup) view.findViewById(R.id.EventRadioGroup);
        viewPager= (ViewPager) view.findViewById(R.id.EventViewPager);
        initView();
        setListener();
        return view;
    }

    private void initView(){
        adapter=new FragmentViewPagerAdapter(getChildFragmentManager(),fragmentList);
        adapter.addFragment(new EventVideoFragment());
        adapter.addFragment(new EventVideoMoreFragment());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
        radioGroup.check(R.id.EventActButton);
    }

    private void setListener(){
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.EventActButton:
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.EventLocalButton:
                        viewPager.setCurrentItem(1);
                        break;
                }
            }
        });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}
            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        radioGroup.check(R.id.EventActButton);
                        break;
                    case 1:
                        radioGroup.check(R.id.EventLocalButton);
                        break;
                }
            }
            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }
}
