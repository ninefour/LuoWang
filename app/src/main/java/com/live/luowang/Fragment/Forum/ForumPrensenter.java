package com.live.luowang.Fragment.Forum;

import android.support.v4.view.ViewPager;
import android.widget.RadioGroup;

/**
 * Created by Chen on 2016/4/6.
 */
public interface ForumPrensenter  {
    void onDetach();
    void onCheckChange(ViewPager viewPager,int checkedId);
    void onPageChange(RadioGroup radioGroup,int position);
}
