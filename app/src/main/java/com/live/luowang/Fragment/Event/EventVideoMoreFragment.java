package com.live.luowang.Fragment.Event;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.Adapter.EventLocalListViewAdapter;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.JsonData.VideoDiscoverData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

/**
 * Created by Chen on 2016/4/5.
 */
public class EventVideoMoreFragment extends Fragment{
    private GridView localListView;
    private VideoDiscoverData discoverData;
    private ImageView loading;
    private AnimationDrawable animationDrawable;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.event_fragment_local_layout,null);
        loading= (ImageView) view.findViewById(R.id.Loading);
        animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        localListView= (GridView) view.findViewById(R.id.EventLocalListView);
        getData();
        return view;
    }
    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            localListView.setAdapter(new EventLocalListViewAdapter(getActivity(),discoverData.getItemList()));
            animationDrawable.stop();
            loading.setVisibility(View.GONE);
        }
    };

    private void getData(){
        StringRequest request=new StringRequest(0, HttpUrl.VIDEODISCOVER, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                discoverData=gson.fromJson(response,VideoDiscoverData.class);
                handler.sendEmptyMessage(1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }

}
