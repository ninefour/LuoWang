package com.live.luowang.Fragment.Event;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.Adapter.EventActListViewAdapter;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.Function.RefreshLayout;
import com.live.luowang.JsonData.VideoData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

/**
 * Created by Chen on 2016/4/5.
 */
public class EventVideoFragment extends Fragment implements RefreshLayout.OnRefreshListener,RefreshLayout.OnLoadListener{
    private VideoData videoData;
    private ListView actListView;
    private EventActListViewAdapter adapter;
    private ImageView loading;
    private AnimationDrawable animationDrawable;
    private RefreshLayout refreshLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.activity_video_layout,null);
        actListView= (ListView) view.findViewById(R.id.AArticleListView);
        loading= (ImageView) view.findViewById(R.id.Loading);
        refreshLayout= (RefreshLayout) view.findViewById(R.id.AArticleSwipeRefresh);
        refreshLayout.setColorSchemeResources(R.color.titleText);
        refreshLayout.setOnRefreshListener(this);
        refreshLayout.setOnLoadListener(this);
        animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        adapter=new EventActListViewAdapter(getActivity());
        actListView.setAdapter(adapter);
        actListView= (ListView) view.findViewById(R.id.AArticleListView);
        getData();
        return view;
    }

    @Override
    public void onRefresh() {
        adapter=new EventActListViewAdapter(getActivity());
        getData();
    }

    @Override
    public void onLoad() {
        getLoadData();
    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            adapter.addListItem(videoData.getIssueList());
            adapter.notifyDataSetChanged();
            animationDrawable.stop();
            loading.setVisibility(View.GONE);
            switch (msg.what){
                case 1:
                    refreshLayout.setRefreshing(false);
                    break;
                case 2:
                    refreshLayout.setLoading(false);
                    break;
            }
        }
    };

    private void getData(){
        StringRequest request=new StringRequest(0, HttpUrl.VIDEO, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                videoData=gson.fromJson(response,VideoData.class);
                handler.sendEmptyMessage(1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }

    private void getLoadData() {
        if (videoData != null) {
            StringRequest request = new StringRequest(0, videoData.getNextPageUrl(), new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Gson gson = new Gson();
                    videoData = gson.fromJson(response, VideoData.class);
                    handler.sendEmptyMessage(2);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {

                }
            });
            MyApplication.getInstance().getQueue().add(request);
        }
    }
}
