package com.live.luowang.Fragment.PeriodicalFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.live.luowang.Adapter.PeriodicalPlayListViewAdapter;
import com.live.luowang.JsonData.NPreodicalData;
import com.live.luowang.MusicServie.MusicPlayingActivity;
import com.live.luowang.MusicServie.MusicProvider;
import com.live.luowang.MusicServie.MusicService;
import com.live.luowang.R;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.Bind;
import butterknife.ButterKnife;


/**
 * Created by Chen on 2016/4/20.
 */
public class PeriodicalPlayFragment extends Fragment {
    @Bind(R.id.PPlayListview)
    ListView listview;
    private TextView content;
    private TextView contentless;
    private ImageView topicImage;
    private TextView moreContent;
    private TextView title;
    private TextView vol;
    private NPreodicalData preodicalData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_periodical_play_layout, null);
        ButterKnife.bind(this, view);
        if (getArguments() != null) {
            preodicalData = (NPreodicalData) getArguments().get("data");
        }
        listview.addHeaderView(getHeader());
        listview.setAdapter(new PeriodicalPlayListViewAdapter(preodicalData.getTracks(), getActivity()));
//        FunctionClass.fixListViewHeight(listview);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if (position==0){return;}
                MusicProvider.setMusic(preodicalData.getTracks());
                Intent intent=new Intent(getActivity(), MusicService.class);
                intent.putExtra("click",true);
                intent.putExtra("position",position-1);
                getActivity().startService(intent);
                Intent intent1=new Intent(getActivity(), MusicPlayingActivity.class);
                getActivity().startActivity(intent1);
            }
        });
        return view;
    }

    public static PeriodicalPlayFragment getInstance(NPreodicalData nPreodicalData) {
        PeriodicalPlayFragment fragment = new PeriodicalPlayFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", nPreodicalData);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }


    private View getHeader() {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.header_pplayer_listview_layout, null);
        content= (TextView) view.findViewById(R.id.PPlayContent);
        contentless= (TextView) view.findViewById(R.id.PPlayContentLess);
        title= (TextView) view.findViewById(R.id.PPlayTitle);
        vol= (TextView) view.findViewById(R.id.PPlayVol);
        moreContent= (TextView) view.findViewById(R.id.PPlayMoreContent);
        topicImage= (ImageView) view.findViewById(R.id.NPeriodicalTopicImage);
        content.setText(preodicalData.getMdesc());
        contentless.setText(preodicalData.getMdesc());
        title.setText(preodicalData.getMname());
        vol.setText("vol." + preodicalData.getMnum());
        ImageLoader.getInstance().displayImage(preodicalData.getMphoto(), topicImage);
        moreContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contentless.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
                moreContent.setVisibility(View.GONE);
            }
        });
        return view;
    }
}
