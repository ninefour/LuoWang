package com.live.luowang.Fragment.Forum;

import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.live.luowang.Adapter.ForumFollowListViewAdapter;
import com.live.luowang.Function.FunctionClass;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.JsonData.ForumData;
import com.live.luowang.MyApplication;
import com.live.luowang.R;

/**
 * Created by Chen on 2016/4/1.
 */
public class ForumFollowFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private SwipeRefreshLayout swipeRefreshLayout;
    private ListView listView;
    private ForumData forumData;
    private ImageView loading;
    private AnimationDrawable animationDrawable;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.forum_fragment_follow_layout,null);
        loading= (ImageView) view.findViewById(R.id.Loading);
        animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        listView= (ListView) view.findViewById(R.id.ForumFollowListView);
        swipeRefreshLayout= (SwipeRefreshLayout) view.findViewById(R.id.ForumFollowSwipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.titleText);
        swipeRefreshLayout.setOnRefreshListener(this);
        onRefresh();
        return view;
    }

    @Override
    public void onRefresh() {
        getData();
    }

    private Handler handler=new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(listView.getHeaderViewsCount()!=1){
                listView.addHeaderView(FunctionClass.getForumFollowHead(getActivity()));
            }
            listView.setAdapter(new ForumFollowListViewAdapter(getActivity(),forumData.getData().getRecommend()));
            animationDrawable.stop();
            loading.setVisibility(View.GONE);
            swipeRefreshLayout.setRefreshing(false);
        }
    };

    private void getData(){
        final StringRequest request=new StringRequest(0, HttpUrl.FORUMFOLLOW, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson=new Gson();
                forumData =gson.fromJson(response,ForumData.class);
                handler.sendEmptyMessage(1);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getInstance().getQueue().add(request);
    }
}
