package com.live.luowang.Fragment.Forum;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import com.live.luowang.Adapter.FragmentViewPagerAdapter;
import com.live.luowang.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Chen on 2016/4/1.
 */
public class ForumFragment extends Fragment implements RadioGroup.OnCheckedChangeListener,ViewPager.OnPageChangeListener{
    private RadioGroup radioGroup;
    private ViewPager viewPager;
    private FragmentViewPagerAdapter adapter;
    private List<Fragment> fragmentList=new ArrayList<>();
    private ForumPrensenter prensenter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_forum_layout,null);
        viewPager= (ViewPager) view.findViewById(R.id.ForumViewPager);
        radioGroup= (RadioGroup) view.findViewById(R.id.ForumRadioGroup);
        prensenter=new ForumPrensenterImpl();
        viewPager.setOffscreenPageLimit(3);
        radioGroup.setOnCheckedChangeListener(this);
        viewPager.addOnPageChangeListener(this);
        initView();
        return view;
    }

    private void initView(){
        adapter=new FragmentViewPagerAdapter(getChildFragmentManager(),fragmentList);
        adapter.addFragment(new ForumFollowFragment());
        adapter.addFragment(new ForumHotFragment());
        adapter.addFragment(new ForumNewestFragment());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(1);
    }

    @Override
    public void onDetach() {super.onDetach();prensenter.onDetach();}

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {prensenter.onCheckChange(viewPager,checkedId);}

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

    @Override
    public void onPageSelected(int position) {prensenter.onPageChange(radioGroup,position);}

    @Override
    public void onPageScrollStateChanged(int state) {}
}
