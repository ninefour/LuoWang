package com.live.luowang.Fragment.Forum;

import android.support.v4.view.ViewPager;
import android.widget.RadioGroup;

import com.live.luowang.R;

/**
 * Created by Chen on 2016/4/6.
 */
public class ForumPrensenterImpl implements ForumPrensenter{
    @Override
    public void onDetach() {
//        try {
//            Field childFragmentManager= Fragment.class.getDeclaredField("mChildFragmentManager");
//            childFragmentManager.setAccessible(true);
//            childFragmentManager.set(this,null);
//        } catch (NoSuchFieldException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public void onCheckChange(ViewPager viewPager,int checkedId) {
        switch (checkedId){
            case R.id.ForumFollowButton:viewPager.setCurrentItem(0);break;
            case R.id.ForumHotButton:viewPager.setCurrentItem(1);break;
            case R.id.ForumNewestButton:viewPager.setCurrentItem(2);break;
        }
    }

    @Override
    public void onPageChange(RadioGroup radioGroup,int position) {
        switch (position) {
            case 0:radioGroup.check(R.id.ForumFollowButton);break;
            case 1:radioGroup.check(R.id.ForumHotButton);break;
            case 2:radioGroup.check(R.id.ForumNewestButton);break;
        }
    }
}
