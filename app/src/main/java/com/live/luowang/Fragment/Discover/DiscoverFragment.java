package com.live.luowang.Fragment.Discover;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.live.luowang.Activity.InnerActivity.FormHotInfoActivity;
import com.live.luowang.Adapter.DiscoverListViewAdapter;
import com.live.luowang.Function.FunctionClass;
import com.live.luowang.Function.HttpUrl;
import com.live.luowang.Function.JsonRequest;
import com.live.luowang.JsonData.DiscoverDailyData;
import com.live.luowang.JsonData.DiscoversData;
import com.live.luowang.R;

/**
 * Created by Chen on 2016/4/1.
 */
public class DiscoverFragment extends Fragment
        implements SwipeRefreshLayout.OnRefreshListener,View.OnClickListener,JsonRequest.CallBack{
    private ListView listView;
    private ImageView userIcon;
    private SwipeRefreshLayout swipeRefreshLayout;
    private DiscoverPrensenter prensenter;
    private ImageView loading;
    private AnimationDrawable animationDrawable;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        prensenter=new DiscoverPrensenterImpl();
        View view=inflater.inflate(R.layout.fragment_discover_layout,null);
        loading= (ImageView) view.findViewById(R.id.Loading);
        animationDrawable= (AnimationDrawable) loading.getDrawable();
        animationDrawable.start();
        listView= (ListView) view.findViewById(R.id.DiscoverListView);
        userIcon= (ImageView) view.findViewById(R.id.DiscoverUserIcon);
        swipeRefreshLayout= (SwipeRefreshLayout) view.findViewById(R.id.DiscoverSwipeRefresh);
        swipeRefreshLayout.setColorSchemeResources(R.color.titleText);
        swipeRefreshLayout.setOnRefreshListener(this);
        JsonRequest.setCallBack(this);
        onRefresh();
        setListener();
        return view;
    }

    private void setListener(){
        userIcon.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        prensenter.onClick(v,getActivity());
    }

    @Override
    public void onRefresh() {
        JsonRequest.getRequest(HttpUrl.DISCOVERHEADERS);
    }

    @Override
    public void call(final Object data, String type) {
        switch (type){
            case "MAINDAILY":{
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent intent=new Intent(getActivity(), FormHotInfoActivity.class);
                        Bundle bundle=new Bundle();
                        bundle.putParcelable("data",((DiscoverDailyData)data).getData().getItems().get(position-1));
                        intent.putExtra("discover",true);
                        intent.putExtras(bundle);
                        getActivity().startActivity(intent);
                    }
                });
                listView.setAdapter(new DiscoverListViewAdapter(getActivity(),((DiscoverDailyData)data).getData().getItems()));
                animationDrawable.stop();
                loading.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                break;
            }
            case "DISCOVERHEADER":{
                if(listView.getHeaderViewsCount()!=1) {
                    listView.addHeaderView(FunctionClass.getDiscoverHead(getActivity(), (DiscoversData) data, swipeRefreshLayout));
                }
                JsonRequest.getRequest(HttpUrl.MAINDAILY);
            }
        }
    }
}
