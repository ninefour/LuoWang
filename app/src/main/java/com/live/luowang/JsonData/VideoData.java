package com.live.luowang.JsonData;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by Chen on 2016/4/22.
 */
public class VideoData{

    private String nextPageUrl;
    private long nextPublishTime;
    private String newestIssueType;

    private List<IssueListBean> issueList;

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public long getNextPublishTime() {
        return nextPublishTime;
    }

    public void setNextPublishTime(long nextPublishTime) {
        this.nextPublishTime = nextPublishTime;
    }

    public String getNewestIssueType() {
        return newestIssueType;
    }

    public void setNewestIssueType(String newestIssueType) {
        this.newestIssueType = newestIssueType;
    }

    public List<IssueListBean> getIssueList() {
        return issueList;
    }

    public void setIssueList(List<IssueListBean> issueList) {
        this.issueList = issueList;
    }

    public static class IssueListBean{
        private long releaseTime;
        private String type;
        private long date;
        private long publishTime;
        private int count;
        /**
         * type : banner1
         * data : {"title":"","description":"","image":"http://img.wdjimg.com/image/video/118e2b4b86831629ba11b45d969f6091_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D860%26shareable%3Dtrue","adTrack":null}
         */

        private List<ItemListBean> itemList;

        public long getReleaseTime() {
            return releaseTime;
        }

        public void setReleaseTime(long releaseTime) {
            this.releaseTime = releaseTime;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public long getDate() {
            return date;
        }

        public void setDate(long date) {
            this.date = date;
        }

        public long getPublishTime() {
            return publishTime;
        }

        public void setPublishTime(long publishTime) {
            this.publishTime = publishTime;
        }

        public int getCount() {
            return count;
        }

        public void setCount(int count) {
            this.count = count;
        }

        public List<ItemListBean> getItemList() {
            return itemList;
        }

        public void setItemList(List<ItemListBean> itemList) {
            this.itemList = itemList;
        }

        public static class ItemListBean{
            private String type;
            /**
             * title :
             * description :
             * image : http://img.wdjimg.com/image/video/118e2b4b86831629ba11b45d969f6091_0_0.jpeg
             * actionUrl : eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D860%26shareable%3Dtrue
             * adTrack : null
             */

            private DataBean data;

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public DataBean getData() {
                return data;
            }

            public void setData(DataBean data) {
                this.data = data;
            }

            public static class DataBean implements Parcelable {

                /**
                 * id : 6724
                 * title : 特条丨「魔兽：崛起」终极燃爆预告
                 * description : 最新 #电影魔兽# 终于不再是大家互相理解的温情画面，而是直面超燃的双方冲突：杜隆坦狂揍吴彦祖饰演的古尔丹！让我们共同期待今年最受期待的好莱坞魔幻史诗巨制吧~From ComingSoon.net
                 * provider : {"name":"YouTube","alias":"youtube","icon":"http://img.wdjimg.com/image/video/fa20228bc5b921e837156923a58713f6_256_256.png"}
                 * category : 预告
                 * author : null
                 * cover : {"feed":"http://img.wdjimg.com/image/video/6d4bde06538d01b9354fb0d9fec1b7fb_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/6d4bde06538d01b9354fb0d9fec1b7fb_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/631e9e4aae3021f1402f64bc25bbca72_0_0.jpeg","sharing":null}
                 * playUrl : http://baobab.wdjcdn.com/14611245402035486.mp4
                 * duration : 140
                 * webUrl : {"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6724","forWeibo":"http://wandou.im/1yvxn2"}
                 * releaseTime : 1461198012000
                 * playInfo : [{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1461125455181_6724_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/14611245402035486.mp4"}]
                 * consumption : {"collectionCount":925,"shareCount":4800,"playCount":0,"replyCount":69}
                 * campaign : null
                 * waterMarks : null
                 * promotion : null
                 * adTrack : null
                 * idx : 0
                 * shareAdTrack : null
                 * favoriteAdTrack : null
                 * webAdTrack : null
                 * date : 1461081600000
                 */

                private int id;
                private String title;
                private String description;
                /**
                 * name : YouTube
                 * alias : youtube
                 * icon : http://img.wdjimg.com/image/video/fa20228bc5b921e837156923a58713f6_256_256.png
                 */

                private ProviderBean provider;
                private String category;
                /**
                 * feed : http://img.wdjimg.com/image/video/6d4bde06538d01b9354fb0d9fec1b7fb_0_0.jpeg
                 * detail : http://img.wdjimg.com/image/video/6d4bde06538d01b9354fb0d9fec1b7fb_0_0.jpeg
                 * blurred : http://img.wdjimg.com/image/video/631e9e4aae3021f1402f64bc25bbca72_0_0.jpeg
                 * sharing : null
                 */

                private CoverBean cover;
                private String playUrl;
                private int duration;
                /**
                 * raw : http://www.wandoujia.com/eyepetizer/detail.html?vid=6724
                 * forWeibo : http://wandou.im/1yvxn2
                 */

                private WebUrlBean webUrl;
                private long releaseTime;
                /**
                 * collectionCount : 925
                 * shareCount : 4800
                 * playCount : 0
                 * replyCount : 69
                 */

                private ConsumptionBean consumption;
                private String waterMarks;
                private int idx;
                private String shareAdTrack;
                private String favoriteAdTrack;
                private String webAdTrack;
                private long date;
                /**
                 * height : 480
                 * width : 854
                 * name : 标清
                 * type : normal
                 * url : http://baobab.wdjcdn.com/1461125455181_6724_854x480.mp4
                 */

                private List<PlayInfoBean> playInfo;
                /**
                 * image : http://img.wdjimg.com/image/video/97a3e4e6b86204bb823847a525ff76b0_0_0.jpeg
                 */

                private String image;
                /**
                 * text : - Apr. 23 -
                 */

                private String text;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public ProviderBean getProvider() {
                    return provider;
                }

                public void setProvider(ProviderBean provider) {
                    this.provider = provider;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public CoverBean getCover() {
                    return cover;
                }

                public void setCover(CoverBean cover) {
                    this.cover = cover;
                }

                public String getPlayUrl() {
                    return playUrl;
                }

                public void setPlayUrl(String playUrl) {
                    this.playUrl = playUrl;
                }

                public int getDuration() {
                    return duration;
                }

                public void setDuration(int duration) {
                    this.duration = duration;
                }

                public WebUrlBean getWebUrl() {
                    return webUrl;
                }

                public void setWebUrl(WebUrlBean webUrl) {
                    this.webUrl = webUrl;
                }

                public long getReleaseTime() {
                    return releaseTime;
                }

                public void setReleaseTime(long releaseTime) {
                    this.releaseTime = releaseTime;
                }

                public ConsumptionBean getConsumption() {
                    return consumption;
                }

                public String getWaterMarks() {
                    return waterMarks;
                }

                public void setWaterMarks(String waterMarks) {
                    this.waterMarks = waterMarks;
                }

                public int getIdx() {
                    return idx;
                }

                public void setIdx(int idx) {
                    this.idx = idx;
                }

                public String getShareAdTrack() {
                    return shareAdTrack;
                }

                public void setShareAdTrack(String shareAdTrack) {
                    this.shareAdTrack = shareAdTrack;
                }

                public String getFavoriteAdTrack() {
                    return favoriteAdTrack;
                }

                public void setFavoriteAdTrack(String favoriteAdTrack) {
                    this.favoriteAdTrack = favoriteAdTrack;
                }

                public String getWebAdTrack() {
                    return webAdTrack;
                }

                public void setWebAdTrack(String webAdTrack) {
                    this.webAdTrack = webAdTrack;
                }

                public long getDate() {
                    return date;
                }

                public void setDate(long date) {
                    this.date = date;
                }

                public List<PlayInfoBean> getPlayInfo() {
                    return playInfo;
                }

                public void setPlayInfo(List<PlayInfoBean> playInfo) {
                    this.playInfo = playInfo;
                }

                public String getImage() {
                    return image;
                }

                public void setImage(String image) {
                    this.image = image;
                }

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public static class ProviderBean implements Parcelable {

                    private String name;
                    private String alias;
                    private String icon;

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getAlias() {
                        return alias;
                    }

                    public void setAlias(String alias) {
                        this.alias = alias;
                    }

                    public String getIcon() {
                        return icon;
                    }

                    public void setIcon(String icon) {
                        this.icon = icon;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.name);
                        dest.writeString(this.alias);
                        dest.writeString(this.icon);
                    }

                    public ProviderBean() {
                    }

                    protected ProviderBean(Parcel in) {
                        this.name = in.readString();
                        this.alias = in.readString();
                        this.icon = in.readString();
                    }

                    public static final Creator<ProviderBean> CREATOR = new Creator<ProviderBean>() {
                        @Override
                        public ProviderBean createFromParcel(Parcel source) {
                            return new ProviderBean(source);
                        }

                        @Override
                        public ProviderBean[] newArray(int size) {
                            return new ProviderBean[size];
                        }
                    };
                }

                public static class CoverBean implements Parcelable {

                    private String feed;
                    private String detail;
                    private String blurred;
                    private String sharing;

                    public String getFeed() {
                        return feed;
                    }

                    public void setFeed(String feed) {
                        this.feed = feed;
                    }

                    public String getDetail() {
                        return detail;
                    }

                    public void setDetail(String detail) {
                        this.detail = detail;
                    }

                    public String getBlurred() {
                        return blurred;
                    }

                    public void setBlurred(String blurred) {
                        this.blurred = blurred;
                    }

                    public String getSharing() {
                        return sharing;
                    }

                    public void setSharing(String sharing) {
                        this.sharing = sharing;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.feed);
                        dest.writeString(this.detail);
                        dest.writeString(this.blurred);
                        dest.writeString(this.sharing);
                    }

                    public CoverBean() {
                    }

                    protected CoverBean(Parcel in) {
                        this.feed = in.readString();
                        this.detail = in.readString();
                        this.blurred = in.readString();
                        this.sharing = in.readString();
                    }

                    public static final Creator<CoverBean> CREATOR = new Creator<CoverBean>() {
                        @Override
                        public CoverBean createFromParcel(Parcel source) {
                            return new CoverBean(source);
                        }

                        @Override
                        public CoverBean[] newArray(int size) {
                            return new CoverBean[size];
                        }
                    };
                }

                public static class WebUrlBean implements Parcelable {

                    private String raw;
                    private String forWeibo;

                    public String getRaw() {
                        return raw;
                    }

                    public void setRaw(String raw) {
                        this.raw = raw;
                    }

                    public String getForWeibo() {
                        return forWeibo;
                    }

                    public void setForWeibo(String forWeibo) {
                        this.forWeibo = forWeibo;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeString(this.raw);
                        dest.writeString(this.forWeibo);
                    }

                    public WebUrlBean() {
                    }

                    protected WebUrlBean(Parcel in) {
                        this.raw = in.readString();
                        this.forWeibo = in.readString();
                    }

                    public static final Creator<WebUrlBean> CREATOR = new Creator<WebUrlBean>() {
                        @Override
                        public WebUrlBean createFromParcel(Parcel source) {
                            return new WebUrlBean(source);
                        }

                        @Override
                        public WebUrlBean[] newArray(int size) {
                            return new WebUrlBean[size];
                        }
                    };
                }

                public static class ConsumptionBean implements Parcelable {

                    private int collectionCount;
                    private int shareCount;
                    private int playCount;
                    private int replyCount;

                    public int getCollectionCount() {
                        return collectionCount;
                    }

                    public void setCollectionCount(int collectionCount) {
                        this.collectionCount = collectionCount;
                    }

                    public int getShareCount() {
                        return shareCount;
                    }

                    public void setShareCount(int shareCount) {
                        this.shareCount = shareCount;
                    }

                    public int getPlayCount() {
                        return playCount;
                    }

                    public void setPlayCount(int playCount) {
                        this.playCount = playCount;
                    }

                    public int getReplyCount() {
                        return replyCount;
                    }

                    public void setReplyCount(int replyCount) {
                        this.replyCount = replyCount;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeInt(this.collectionCount);
                        dest.writeInt(this.shareCount);
                        dest.writeInt(this.playCount);
                        dest.writeInt(this.replyCount);
                    }

                    public ConsumptionBean() {
                    }

                    protected ConsumptionBean(Parcel in) {
                        this.collectionCount = in.readInt();
                        this.shareCount = in.readInt();
                        this.playCount = in.readInt();
                        this.replyCount = in.readInt();
                    }

                    public static final Creator<ConsumptionBean> CREATOR = new Creator<ConsumptionBean>() {
                        @Override
                        public ConsumptionBean createFromParcel(Parcel source) {
                            return new ConsumptionBean(source);
                        }

                        @Override
                        public ConsumptionBean[] newArray(int size) {
                            return new ConsumptionBean[size];
                        }
                    };
                }

                public static class PlayInfoBean implements Parcelable {

                    private int height;
                    private int width;
                    private String name;
                    private String type;
                    private String url;

                    public int getHeight() {
                        return height;
                    }

                    public void setHeight(int height) {
                        this.height = height;
                    }

                    public int getWidth() {
                        return width;
                    }

                    public void setWidth(int width) {
                        this.width = width;
                    }

                    public String getName() {
                        return name;
                    }

                    public void setName(String name) {
                        this.name = name;
                    }

                    public String getType() {
                        return type;
                    }

                    public void setType(String type) {
                        this.type = type;
                    }

                    public String getUrl() {
                        return url;
                    }

                    public void setUrl(String url) {
                        this.url = url;
                    }

                    @Override
                    public int describeContents() {
                        return 0;
                    }

                    @Override
                    public void writeToParcel(Parcel dest, int flags) {
                        dest.writeInt(this.height);
                        dest.writeInt(this.width);
                        dest.writeString(this.name);
                        dest.writeString(this.type);
                        dest.writeString(this.url);
                    }

                    public PlayInfoBean() {
                    }

                    protected PlayInfoBean(Parcel in) {
                        this.height = in.readInt();
                        this.width = in.readInt();
                        this.name = in.readString();
                        this.type = in.readString();
                        this.url = in.readString();
                    }

                    public static final Creator<PlayInfoBean> CREATOR = new Creator<PlayInfoBean>() {
                        @Override
                        public PlayInfoBean createFromParcel(Parcel source) {
                            return new PlayInfoBean(source);
                        }

                        @Override
                        public PlayInfoBean[] newArray(int size) {
                            return new PlayInfoBean[size];
                        }
                    };
                }

                public DataBean() {
                }

                @Override
                public int describeContents() {
                    return 0;
                }

                @Override
                public void writeToParcel(Parcel dest, int flags) {
                    dest.writeInt(this.id);
                    dest.writeString(this.title);
                    dest.writeString(this.description);
                    dest.writeParcelable(this.provider, flags);
                    dest.writeString(this.category);
                    dest.writeParcelable(this.cover, flags);
                    dest.writeString(this.playUrl);
                    dest.writeInt(this.duration);
                    dest.writeParcelable(this.webUrl, flags);
                    dest.writeLong(this.releaseTime);
                    dest.writeParcelable(this.consumption, flags);
                    dest.writeString(this.waterMarks);
                    dest.writeInt(this.idx);
                    dest.writeString(this.shareAdTrack);
                    dest.writeString(this.favoriteAdTrack);
                    dest.writeString(this.webAdTrack);
                    dest.writeLong(this.date);
                    dest.writeTypedList(playInfo);
                    dest.writeString(this.image);
                    dest.writeString(this.text);
                }

                protected DataBean(Parcel in) {
                    this.id = in.readInt();
                    this.title = in.readString();
                    this.description = in.readString();
                    this.provider = in.readParcelable(ProviderBean.class.getClassLoader());
                    this.category = in.readString();
                    this.cover = in.readParcelable(CoverBean.class.getClassLoader());
                    this.playUrl = in.readString();
                    this.duration = in.readInt();
                    this.webUrl = in.readParcelable(WebUrlBean.class.getClassLoader());
                    this.releaseTime = in.readLong();
                    this.consumption = in.readParcelable(ConsumptionBean.class.getClassLoader());
                    this.waterMarks = in.readString();
                    this.idx = in.readInt();
                    this.shareAdTrack = in.readString();
                    this.favoriteAdTrack = in.readString();
                    this.webAdTrack = in.readString();
                    this.date = in.readLong();
                    this.playInfo = in.createTypedArrayList(PlayInfoBean.CREATOR);
                    this.image = in.readString();
                    this.text = in.readString();
                }

                public static final Creator<DataBean> CREATOR = new Creator<DataBean>() {
                    @Override
                    public DataBean createFromParcel(Parcel source) {
                        return new DataBean(source);
                    }

                    @Override
                    public DataBean[] newArray(int size) {
                        return new DataBean[size];
                    }
                };
            }
        }
    }
}
