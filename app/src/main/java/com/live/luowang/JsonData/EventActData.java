package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/13.
 */
public class EventActData {

    /**
     * err_code : 0
     * msg :
     * data : {"items":[{"event_id":"13156","subject":"奉天乐队新专辑《勇往直前》全国巡演","time":"2015-09-26","fee":"40元 - 60元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408203","latitude":"37.549402","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/13156/56025798b07ec.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"12818","subject":"不可撤销《撒哈拉寻宝记》2015全国巡演","time":"2015-07-24","fee":"60元 - 80元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408178","latitude":"37.549285","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/12818/601040bb131db3d614d140f9cd43c242.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"10692","subject":"【烟台站】张尕怂2014全国巡演","time":"2014-06-21","fee":"40.00元","city_id":"101443","city_name":"烟台","address":"芝罘区 南大街300号一楼西2号（壹通国际对面，恒丰银行西侧）","longitude":"121.409934","latitude":"37.543926","place_id":"100209","place_name":"小灯塔咖啡馆","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/10692/8640ff6ca914c9855203ac93e4c05119.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"10255","subject":"【烟台站】布衣乐队新专辑《出发》2014巡演","time":"2014-05-09","fee":"70.00元 - 90.00元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408178","latitude":"37.549285","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/10255/d093e56ef752adba41e5eaf26d1960a7.jpg?imageView2/1/w/180/h/225","is_recommend":"no"}],"pages":1,"current_page":1}
     */

    private int err_code;
    private String msg;
    /**
     * items : [{"event_id":"13156","subject":"奉天乐队新专辑《勇往直前》全国巡演","time":"2015-09-26","fee":"40元 - 60元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408203","latitude":"37.549402","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/13156/56025798b07ec.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"12818","subject":"不可撤销《撒哈拉寻宝记》2015全国巡演","time":"2015-07-24","fee":"60元 - 80元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408178","latitude":"37.549285","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/12818/601040bb131db3d614d140f9cd43c242.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"10692","subject":"【烟台站】张尕怂2014全国巡演","time":"2014-06-21","fee":"40.00元","city_id":"101443","city_name":"烟台","address":"芝罘区 南大街300号一楼西2号（壹通国际对面，恒丰银行西侧）","longitude":"121.409934","latitude":"37.543926","place_id":"100209","place_name":"小灯塔咖啡馆","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/10692/8640ff6ca914c9855203ac93e4c05119.jpg?imageView2/1/w/180/h/225","is_recommend":"no"},{"event_id":"10255","subject":"【烟台站】布衣乐队新专辑《出发》2014巡演","time":"2014-05-09","fee":"70.00元 - 90.00元","city_id":"101443","city_name":"烟台","address":"烟台市芝罘区朝阳街16号（中段）","longitude":"121.408178","latitude":"37.549285","place_id":"100108","place_name":"烟台哈瓦那酒吧","poster":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/10255/d093e56ef752adba41e5eaf26d1960a7.jpg?imageView2/1/w/180/h/225","is_recommend":"no"}]
     * pages : 1
     * current_page : 1
     */

    private DataBean data;

    public int getErr_code() {
        return err_code;
    }

    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int pages;
        private int current_page;
        /**
         * event_id : 13156
         * subject : 奉天乐队新专辑《勇往直前》全国巡演
         * time : 2015-09-26
         * fee : 40元 - 60元
         * city_id : 101443
         * city_name : 烟台
         * address : 烟台市芝罘区朝阳街16号（中段）
         * longitude : 121.408203
         * latitude : 37.549402
         * place_id : 100108
         * place_name : 烟台哈瓦那酒吧
         * poster : http://7xkszy.com2.z0.glb.qiniucdn.com/pics/event/13156/56025798b07ec.jpg?imageView2/1/w/180/h/225
         * is_recommend : no
         */

        private List<ItemsBean> items;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public int getCurrent_page() {
            return current_page;
        }

        public void setCurrent_page(int current_page) {
            this.current_page = current_page;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            private String event_id;
            private String subject;
            private String time;
            private String fee;
            private String city_id;
            private String city_name;
            private String address;
            private String longitude;
            private String latitude;
            private String place_id;
            private String place_name;
            private String poster;
            private String is_recommend;

            public String getEvent_id() {
                return event_id;
            }

            public void setEvent_id(String event_id) {
                this.event_id = event_id;
            }

            public String getSubject() {
                return subject;
            }

            public void setSubject(String subject) {
                this.subject = subject;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getFee() {
                return fee;
            }

            public void setFee(String fee) {
                this.fee = fee;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getCity_name() {
                return city_name;
            }

            public void setCity_name(String city_name) {
                this.city_name = city_name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getPlace_id() {
                return place_id;
            }

            public void setPlace_id(String place_id) {
                this.place_id = place_id;
            }

            public String getPlace_name() {
                return place_name;
            }

            public void setPlace_name(String place_name) {
                this.place_name = place_name;
            }

            public String getPoster() {
                return poster;
            }

            public void setPoster(String poster) {
                this.poster = poster;
            }

            public String getIs_recommend() {
                return is_recommend;
            }

            public void setIs_recommend(String is_recommend) {
                this.is_recommend = is_recommend;
            }
        }
    }
}
