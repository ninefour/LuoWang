package com.live.luowang.JsonData;

/**
 * Created by Chen on 2016/4/26.
 */
public class TrackBean {
        private String id;
        private String resourcecode;
        private String songer;
        private String songname;
        private String  songalbum;
        private String filename;
        private String songphoto;
        private String remarks;
        private String del_flag;
        private String  create_by;
        private String create_date;
        private String update_by;
        private String update_date;
        private String filename192;
        private String filename320;
        private String time;
        private String thumbnail_url;
        private String fsize;
        private Object ggid;
        private Object ypid;
        private String mtype;
        private String uid;
        private Object lyrics;
        private String status;
        private String flag;
        private Object note;
        private String create_time;
        private String update_time;
        private String publish_time;
        private String vol_id;
        private Object musician_id;
        private Object banner_id;
        private String fee_tag;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getResourcecode() {
            return resourcecode;
        }

        public void setResourcecode(String resourcecode) {
            this.resourcecode = resourcecode;
        }

        public String getSonger() {
            return songer;
        }

        public void setSonger(String songer) {
            this.songer = songer;
        }

        public String getSongname() {
            return songname;
        }

        public void setSongname(String songname) {
            this.songname = songname;
        }

        public String getSongalbum() {
            return songalbum;
        }

        public void setSongalbum(String songalbum) {
            this.songalbum = songalbum;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getSongphoto() {
            return songphoto;
        }

        public void setSongphoto(String songphoto) {
            this.songphoto = songphoto;
        }

        public String getRemarks() {
            return remarks;
        }

        public void setRemarks(String remarks) {
            this.remarks = remarks;
        }

        public String getDel_flag() {
            return del_flag;
        }

        public void setDel_flag(String del_flag) {
            this.del_flag = del_flag;
        }

        public String getCreate_by() {
            return create_by;
        }

        public void setCreate_by(String create_by) {
            this.create_by = create_by;
        }

        public String getCreate_date() {
            return create_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public String getUpdate_by() {
            return update_by;
        }

        public void setUpdate_by(String update_by) {
            this.update_by = update_by;
        }

        public String getUpdate_date() {
            return update_date;
        }

        public void setUpdate_date(String update_date) {
            this.update_date = update_date;
        }

        public String getFilename192() {
            return filename192;
        }

        public void setFilename192(String filename192) {
            this.filename192 = filename192;
        }

        public String getFilename320() {
            return filename320;
        }

        public void setFilename320(String filename320) {
            this.filename320 = filename320;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }

        public String getFsize() {
            return fsize;
        }

        public void setFsize(String fsize) {
            this.fsize = fsize;
        }

        public Object getGgid() {
            return ggid;
        }

        public void setGgid(Object ggid) {
            this.ggid = ggid;
        }

        public Object getYpid() {
            return ypid;
        }

        public void setYpid(Object ypid) {
            this.ypid = ypid;
        }

        public String getMtype() {
            return mtype;
        }

        public void setMtype(String mtype) {
            this.mtype = mtype;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public Object getLyrics() {
            return lyrics;
        }

        public void setLyrics(Object lyrics) {
            this.lyrics = lyrics;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public Object getNote() {
            return note;
        }

        public void setNote(Object note) {
            this.note = note;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public String getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(String update_time) {
            this.update_time = update_time;
        }

        public String getPublish_time() {
            return publish_time;
        }

        public void setPublish_time(String publish_time) {
            this.publish_time = publish_time;
        }

        public String getVol_id() {
            return vol_id;
        }

        public void setVol_id(String vol_id) {
            this.vol_id = vol_id;
        }

        public Object getMusician_id() {
            return musician_id;
        }

        public void setMusician_id(Object musician_id) {
            this.musician_id = musician_id;
        }

        public Object getBanner_id() {
            return banner_id;
        }

        public void setBanner_id(Object banner_id) {
            this.banner_id = banner_id;
        }

        public String getFee_tag() {
            return fee_tag;
        }

        public void setFee_tag(String fee_tag) {
            this.fee_tag = fee_tag;
        }
    }
