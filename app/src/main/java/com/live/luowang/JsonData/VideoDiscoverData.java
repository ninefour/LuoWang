package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/25.
 */
public class VideoDiscoverData {

    /**
     * itemList : [{"type":"horizontalScrollCard","data":{"itemList":[{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/cd32e22c663fff80b22d4b428fb02bf5_0_0.jpeg","actionUrl":"eyepetizer://recommend/","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/3096abf09bd4a319f1a628d8c71296f9_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%202.0&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D862%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/118e2b4b86831629ba11b45d969f6091_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D860%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/a6cf1e605692edfd87f8259ef2ec60f3_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D852%26shareable%3Dtrue","adTrack":[{"organization":"admaster","viewUrl":"http://v.admaster.com.cn/i/a64875,b985185,c3101,i0,m202,h","clickUrl":"http://c.admaster.com.cn/c/a64875,b985185,c3101,i0,m101,h"}]}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/42889d4b41955eb7037983c01f6897a8_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Fcollection.html%3Fname%3Dminecraft%26ineyepetizerapp%3Dtrue%26shareable%3Dtrue","adTrack":null}}],"count":5}},{"type":"topPgc","data":{"title":"－ 优质作者 －","pgcList":[{"id":22,"icon":"http://img.wdjimg.com/image/video/2faf2139c4b655e56a54bdb2ee06e0b4_0_0.jpeg","name":"日食记","description":"知名美食博主。@姜老刀 和小伙伴们一起做饭吃饭的地儿。","actionUrl":"eyepetizer://pgc/detail/22/?title=%E6%97%A5%E9%A3%9F%E8%AE%B0"},{"id":170,"icon":"http://img.wdjimg.com/image/video/f00415e8c5b784e1ec42292e97d0bcbd_0_0.jpeg","name":"一条视频","description":"生活丨潮流丨文艺","actionUrl":"eyepetizer://pgc/detail/170/?title=%E4%B8%80%E6%9D%A1%E8%A7%86%E9%A2%91"},{"id":2,"icon":"http://img.wdjimg.com/image/video/2e57fb7eea469d9455d37d49e9270b25_0_0.jpeg","name":"二更","description":"发现身边不知道的美","actionUrl":"eyepetizer://pgc/detail/2/?title=%E4%BA%8C%E6%9B%B4"},{"id":6,"icon":"http://img.wdjimg.com/image/video/3cf7e87b7ec11cb9174400014bb945e3_0_0.jpeg","name":"知咚","description":"关注知咚原创视频，看到生活的另一种可能。","actionUrl":"eyepetizer://pgc/detail/6/?title=%E7%9F%A5%E5%92%9A"}],"count":4,"actionUrl":"eyepetizer://pgc/feature/?title=%EF%BC%8D%20%E4%BC%98%E8%B4%A8%E4%BD%9C%E8%80%85%20%EF%BC%8D"}},{"type":"squareCard","data":{"id":0,"title":"#专题","image":"http://img.wdjimg.com/image/video/129fec92896cf9c52cc12ad135d87e8a_0_0.png","actionUrl":"eyepetizer://campaign/list/?title=%E4%B8%93%E9%A2%98"}},{"type":"squareCard","data":{"id":1,"title":"#作者","image":"http://img.wdjimg.com/image/video/1f814c1628741f23deff8cd158c03835_0_0.png","actionUrl":"eyepetizer://pgc/feature/?title=%EF%BC%8D%20%E4%BC%98%E8%B4%A8%E4%BD%9C%E8%80%85%20%EF%BC%8D"}},{"type":"squareCard","data":{"id":2,"title":"#创意","image":"http://img.wdjimg.com/image/video/9ead6e8356e42c3382a316f22bc5924e_0_0.jpeg","actionUrl":"eyepetizer://category/2/?title=%E5%88%9B%E6%84%8F"}},{"type":"squareCard","data":{"id":18,"title":"#运动","image":"http://img.wdjimg.com/image/video/b6c4d309809573728da5e627effe1738_0_0.jpeg","actionUrl":"eyepetizer://category/18/?title=%E8%BF%90%E5%8A%A8"}},{"type":"squareCard","data":{"id":6,"title":"#旅行","image":"http://img.wdjimg.com/image/video/3adcb2488d0431ac90193523133a76b7_0_0.jpeg","actionUrl":"eyepetizer://category/6/?title=%E6%97%85%E8%A1%8C"}},{"type":"squareCard","data":{"id":12,"title":"#剧情","image":"http://img.wdjimg.com/image/video/f209d8f25c101aef55b64cfe9a245cf6_0_0.jpeg","actionUrl":"eyepetizer://category/12/?title=%E5%89%A7%E6%83%85"}},{"type":"squareCard","data":{"id":10,"title":"#动画","image":"http://img.wdjimg.com/image/video/821b37d0daaee553906df79237dae6f5_0_0.jpeg","actionUrl":"eyepetizer://category/10/?title=%E5%8A%A8%E7%94%BB"}},{"type":"squareCard","data":{"id":14,"title":"#广告","image":"http://img.wdjimg.com/image/video/5b33fb58f825a6dc893e56322e5f55c4_0_0.jpeg","actionUrl":"eyepetizer://category/14/?title=%E5%B9%BF%E5%91%8A"}},{"type":"squareCard","data":{"id":20,"title":"#音乐","image":"http://img.wdjimg.com/image/video/6af30fc4e6a96856619a9f2e8e745bf5_0_0.jpeg","actionUrl":"eyepetizer://category/20/?title=%E9%9F%B3%E4%B9%90"}},{"type":"squareCard","data":{"id":4,"title":"#开胃","image":"http://img.wdjimg.com/image/video/929c4231b8f3ed6cb6c2180fbe930f33_0_0.jpeg","actionUrl":"eyepetizer://category/4/?title=%E5%BC%80%E8%83%83"}},{"type":"squareCard","data":{"id":8,"title":"#预告","image":"http://img.wdjimg.com/image/video/f2b80f69c0b10fe484aacfc24d62645c_0_0.jpeg","actionUrl":"eyepetizer://category/8/?title=%E9%A2%84%E5%91%8A"}},{"type":"squareCard","data":{"id":16,"title":"#综合","image":"http://img.wdjimg.com/image/video/6676c31afce1e79b40ad3d6c2c9ecc57_0_0.jpeg","actionUrl":"eyepetizer://category/16/?title=%E7%BB%BC%E5%90%88"}},{"type":"squareCard","data":{"id":22,"title":"#记录","image":"http://img.wdjimg.com/image/video/bbbf63542576f27cc0ab1222601894fa_0_0.jpeg","actionUrl":"eyepetizer://category/22/?title=%E8%AE%B0%E5%BD%95"}},{"type":"squareCard","data":{"id":24,"title":"#时尚","image":"http://img.wdjimg.com/image/video/b1dc8cc45addc496300c4dc87ba16e52_0_0.jpeg","actionUrl":"eyepetizer://category/24/?title=%E6%97%B6%E5%B0%9A"}}]
     * count : 16
     * total : 0
     * nextPageUrl : null
     */

    private int count;
    private int total;
    private Object nextPageUrl;
    /**
     * type : horizontalScrollCard
     * data : {"itemList":[{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/cd32e22c663fff80b22d4b428fb02bf5_0_0.jpeg","actionUrl":"eyepetizer://recommend/","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/3096abf09bd4a319f1a628d8c71296f9_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%202.0&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D862%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/118e2b4b86831629ba11b45d969f6091_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D860%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/a6cf1e605692edfd87f8259ef2ec60f3_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D852%26shareable%3Dtrue","adTrack":[{"organization":"admaster","viewUrl":"http://v.admaster.com.cn/i/a64875,b985185,c3101,i0,m202,h","clickUrl":"http://c.admaster.com.cn/c/a64875,b985185,c3101,i0,m101,h"}]}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/42889d4b41955eb7037983c01f6897a8_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Fcollection.html%3Fname%3Dminecraft%26ineyepetizerapp%3Dtrue%26shareable%3Dtrue","adTrack":null}}],"count":5}
     */

    private List<ItemListBean> itemList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public Object getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(Object nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public List<ItemListBean> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemListBean> itemList) {
        this.itemList = itemList;
    }

    public static class ItemListBean {
        private String type;
        /**
         * itemList : [{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/cd32e22c663fff80b22d4b428fb02bf5_0_0.jpeg","actionUrl":"eyepetizer://recommend/","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/3096abf09bd4a319f1a628d8c71296f9_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%202.0&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D862%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/118e2b4b86831629ba11b45d969f6091_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D860%26shareable%3Dtrue","adTrack":null}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/a6cf1e605692edfd87f8259ef2ec60f3_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Farticle.html%3Fnid%3D852%26shareable%3Dtrue","adTrack":[{"organization":"admaster","viewUrl":"http://v.admaster.com.cn/i/a64875,b985185,c3101,i0,m202,h","clickUrl":"http://c.admaster.com.cn/c/a64875,b985185,c3101,i0,m101,h"}]}},{"type":"banner1","data":{"title":"","description":"","image":"http://img.wdjimg.com/image/video/42889d4b41955eb7037983c01f6897a8_0_0.jpeg","actionUrl":"eyepetizer://webview/?title=%E5%BC%80%E7%9C%BC%E4%B8%93%E9%A2%98&url=http%3A%2F%2Fwww.wandoujia.com%2Feyepetizer%2Fcollection.html%3Fname%3Dminecraft%26ineyepetizerapp%3Dtrue%26shareable%3Dtrue","adTrack":null}}]
         * count : 5
         */

        private DataBean data;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean {

            /**
             * id : 2
             * title : #创意
             * image : http://img.wdjimg.com/image/video/9ead6e8356e42c3382a316f22bc5924e_0_0.jpeg
             * actionUrl : eyepetizer://category/2/?title=%E5%88%9B%E6%84%8F
             */

            private int id;
            private String title;
            private String image;
            private String actionUrl;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getActionUrl() {
                return actionUrl;
            }

            public void setActionUrl(String actionUrl) {
                this.actionUrl = actionUrl;
            }
        }
    }
}
