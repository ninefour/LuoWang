package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/25.
 */
public class VideoMoreData {

    /**
     * itemList : [{"type":"video","data":{"id":6316,"title":"异样的宇宙情节：浮岛","description":"「浮岛」依托装置属性的建筑体，思考着物态背后的观念。即当我们不断刷新城市进程，在钢筋混泥土之上发展文明的同时，那些被替换遗忘的人、物或者历史，都在这些既成物背后浮躁不安，沉沦逝去。From @Roooyi","provider":{"name":"优酷","alias":"youku","icon":"http://img.wdjimg.com/mms/icon/v1/3/2d/dc14dd1e40b8e561eae91584432262d3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/c82852147bc84da735c566ae5000a76b_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/14593222096744.mp4","duration":605,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6316","forWeibo":"http://wandou.im/1t4uh4"},"releaseTime":1461470400000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1459397364253_6316_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1459397020727_6316_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/14593222096744.mp4"}],"consumption":{"collectionCount":252,"shareCount":135,"playCount":34902,"replyCount":25},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461470400000}},{"type":"video","data":{"id":6344,"title":"把地图嵌入夹克之中","description":"在夜里骑自行车十分危险，有人聪明地在车上安装光源避免事故。而外国人则想得更远：他们直接做了一件会发光的夹克，更妙的是，这件夹克很智能，可以充当GPS导航，并向其他行人表明你的行驶方向。From vodafonel","provider":{"name":"YouTube","alias":"youtube","icon":"http://img.wdjimg.com/image/video/fa20228bc5b921e837156923a58713f6_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/a855533f3998a73d7605f2218c7b5d4d_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/a855533f3998a73d7605f2218c7b5d4d_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/28f6981d30cd9a1a3bbe835df4523da6_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/146143235536512345.mp4","duration":97,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6344","forWeibo":"http://wandou.im/1tebct"},"releaseTime":1461470400000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1461433186698_6344_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1461433122514_6344_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/146143235536512345.mp4"}],"consumption":{"collectionCount":1078,"shareCount":1391,"playCount":41692,"replyCount":67},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461470400000}},{"type":"video","data":{"id":6474,"title":"哈德良铜像制作","description":"短片是为 2015 年在以色列博物馆举行的「哈德良展」所做，它以定格动画和 2D 动画形式展示了以脱蜡技术制作哈德良像的过程。其中所用的头是在公元 117-138 年发现的哈德良像的复刻品。From Renana Aldor & Kobi Vogm","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/4ad443c365301179eb658032b57ec0d4_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/4ad443c365301179eb658032b57ec0d4_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/6694e3a7bdcd42366622e7e4bf8ec340_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1460011848594500994042.mp4","duration":259,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6474","forWeibo":"http://wandou.im/1vcpe0"},"releaseTime":1461427200000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1460063841371_6474_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1460063427644_6474_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1460011848594500994042.mp4"}],"consumption":{"collectionCount":429,"shareCount":268,"playCount":32079,"replyCount":10},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461427200000}},{"type":"video","data":{"id":4698,"title":"建筑的力量：无言而壮阔","description":"这支 CG 制作的短片以慢而有序的方式展示了建筑的力量。沉默黢黑的钢铁建筑，缓缓转动的洁白风车，纷飞的繁花，林间的小屋\u2026\u2026静下心来，看完这支短片，它带来的会是视觉和听觉的双重盛宴。From Alex Roma","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/2f7b5f8acf2ed0514b9c3366bdbb9b19_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/2f7b5f8acf2ed0514b9c3366bdbb9b19_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/5795d50e7adb8ad616dbb0a2cbf56cec_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/14522338822204.mp4","duration":748,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=4698","forWeibo":"http://wandou.im/16jnt7"},"releaseTime":1461384000000,"playInfo":[{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/14522338822204.mp4"}],"consumption":{"collectionCount":1146,"shareCount":736,"playCount":43817,"replyCount":26},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461384000000}},{"type":"video","data":{"id":6726,"title":"视效作品集锦：眼花缭乱","description":"平面设计师及创意总监 Ash Thorp 曾参与制作过「安德的游戏」、「普罗米修斯」、「X战警：第一战」、「超凡蜘蛛侠2」等电影。这支短片是 Ash Thorp 的作品集锦，一起来感受大神的深厚功底吧~From Ash Thorp","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/a3c97d7d4914044e3073eb5eda8f7848_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/a3c97d7d4914044e3073eb5eda8f7848_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/432fa8672e06850df8c91395417e1bc8_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1461131422879a.mp4","duration":98,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6726","forWeibo":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6726"},"releaseTime":1461254400000,"playInfo":[],"consumption":{"collectionCount":933,"shareCount":398,"playCount":9864,"replyCount":29},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461254400000}},{"type":"video","data":{"id":6712,"title":"创意产业的打怪升级之路","description":"短片是 2016届在多伦多举行的全球创意集会 FITC（Future. Innovation. Technology. Creativity）的开幕片。短片以流畅的动效和鲜亮的颜色搭配明确地响应主旨：It's time to level up。From FITC","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/8ba7e4708a5a2f0c63ad327afcd7d528_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/8ba7e4708a5a2f0c63ad327afcd7d528_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/1ad8133a611a5b242df343bdcfb6542a_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1461044415352f.mp4","duration":175,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6712","forWeibo":"http://wandou.im/1ynahz"},"releaseTime":1461081600000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1461048291497_6712_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1461048153168_6712_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1461044415352f.mp4"}],"consumption":{"collectionCount":1708,"shareCount":1889,"playCount":5004,"replyCount":48},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461081600000}},{"type":"video","data":{"id":6280,"title":"「Wide Open」是这么做出来的","description":"还记得之前「Wide Open」的 MV 吗？这支短片将揭示在这支创意十足，特效良心（以及导致各位密恐患者不适）的 MV 是怎样做出来的。看完想给幕后团队点个赞~From The Mill","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/1dcd394a6bd1f656f0b94aef6009d0c4_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/1dcd394a6bd1f656f0b94aef6009d0c4_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/bf4f282f8f7770b04ac4af3e2bf723ee_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1459157086903476164961.mp4","duration":245,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6280","forWeibo":"http://wandou.im/1smfle"},"releaseTime":1460865600000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1459393889132_6280_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1459393704828_6280_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1459157086903476164961.mp4"}],"consumption":{"collectionCount":1409,"shareCount":1138,"playCount":59726,"replyCount":43},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1460865600000}},{"type":"video","data":{"id":6356,"title":"炫酷 CG：科技产业未来","description":"一支有着满满的科技范和未来感的短片~BMW i 用炫酷的CG画面向人们展示了未来产业的面貌：全自动化生产，以机器制造机器，以机器人制造机器人\u2026\u2026虽然还只是 CG 概念，但看着依然让人心驰神往呢~From 908 // berlin","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/91b58d217b4fe5920432d7184639d705_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/91b58d217b4fe5920432d7184639d705_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/7852c759710ee2a5ddf5812ab5148d87_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1459413832207498456379.mp4","duration":90,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6356","forWeibo":"http://wandou.im/1tfc7o"},"releaseTime":1460822400000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1459415545678_6356_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1459415380355_6356_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1459413832207498456379.mp4"}],"consumption":{"collectionCount":1332,"shareCount":795,"playCount":67770,"replyCount":36},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1460822400000}},{"type":"video","data":{"id":6638,"title":"常理不足为奇，违背常理才有趣","description":"谁说用飞镖扎气球就一定会破？苹果落到球拍上就会反弹回来？在这里，常理不足为奇，违背常理才有趣！短片展现了各种违背常理的脑洞大开结局，打破常规，从开发脑洞做起~From Animasjonsdepartementet","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/dc6bfde7625692511bd32f67c4bcca2b_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/dc6bfde7625692511bd32f67c4bcca2b_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/a7db7d0ec9b132ddf134a9a9395559e1_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1460708787995513095622.mp4","duration":197,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6638","forWeibo":"http://wandou.im/1xkj6o"},"releaseTime":1460779200000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1460709054836_6638_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1460708925320_6638_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1460708787995513095622.mp4"}],"consumption":{"collectionCount":2112,"shareCount":1045,"playCount":98692,"replyCount":36},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1460779200000}},{"type":"video","data":{"id":6596,"title":"脑洞突破天际的街头表演","description":"提示：这不是一个普通的游乐园游玩短片，也不是一个小孩看街头表演的纪实故事。它到底是什么，可能要你看到最后，自己体会。（如果你看完一脸懵逼，放心，你并不是孤独的一个人。）From Daniel Moshel","provider":{"name":"Vimeo","alias":"vimeo","icon":"http://img.wdjimg.com/image/video/c3ad630be461cbb081649c9e21d6cbe3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/63e467f2dfa0e77a42ebffc0eb0dd2a3_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/63e467f2dfa0e77a42ebffc0eb0dd2a3_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/b755cfaffd97aabc205dbcce829f7588_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/1460536767820w.mp4","duration":341,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6596","forWeibo":"http://wandou.im/1x0scm"},"releaseTime":1460649600000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1460538098800_6596_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1460537232477_6596_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/1460536767820w.mp4"}],"consumption":{"collectionCount":1678,"shareCount":1710,"playCount":87051,"replyCount":91},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1460649600000}}]
     * count : 10
     * total : 0
     * nextPageUrl : http://baobab.wandoujia.com/api/v3/videos?start=10&num=10&categoryId=2&strategy=date
     */

    private int count;
    private int total;
    private String nextPageUrl;
    /**
     * type : video
     * data : {"id":6316,"title":"异样的宇宙情节：浮岛","description":"「浮岛」依托装置属性的建筑体，思考着物态背后的观念。即当我们不断刷新城市进程，在钢筋混泥土之上发展文明的同时，那些被替换遗忘的人、物或者历史，都在这些既成物背后浮躁不安，沉沦逝去。From @Roooyi","provider":{"name":"优酷","alias":"youku","icon":"http://img.wdjimg.com/mms/icon/v1/3/2d/dc14dd1e40b8e561eae91584432262d3_256_256.png"},"category":"创意","author":null,"cover":{"feed":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/c82852147bc84da735c566ae5000a76b_0_0.jpeg","sharing":null},"playUrl":"http://baobab.wdjcdn.com/14593222096744.mp4","duration":605,"webUrl":{"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6316","forWeibo":"http://wandou.im/1t4uh4"},"releaseTime":1461470400000,"playInfo":[{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1459397364253_6316_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1459397020727_6316_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/14593222096744.mp4"}],"consumption":{"collectionCount":252,"shareCount":135,"playCount":34902,"replyCount":25},"campaign":null,"waterMarks":null,"promotion":null,"adTrack":null,"idx":0,"shareAdTrack":null,"favoriteAdTrack":null,"webAdTrack":null,"date":1461470400000}
     */

    private List<ItemListBean> itemList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }

    public void setNextPageUrl(String nextPageUrl) {
        this.nextPageUrl = nextPageUrl;
    }

    public List<ItemListBean> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemListBean> itemList) {
        this.itemList = itemList;
    }

    public static class ItemListBean {
        private String type;
        /**
         * id : 6316
         * title : 异样的宇宙情节：浮岛
         * description : 「浮岛」依托装置属性的建筑体，思考着物态背后的观念。即当我们不断刷新城市进程，在钢筋混泥土之上发展文明的同时，那些被替换遗忘的人、物或者历史，都在这些既成物背后浮躁不安，沉沦逝去。From @Roooyi
         * provider : {"name":"优酷","alias":"youku","icon":"http://img.wdjimg.com/mms/icon/v1/3/2d/dc14dd1e40b8e561eae91584432262d3_256_256.png"}
         * category : 创意
         * author : null
         * cover : {"feed":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","detail":"http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg","blurred":"http://img.wdjimg.com/image/video/c82852147bc84da735c566ae5000a76b_0_0.jpeg","sharing":null}
         * playUrl : http://baobab.wdjcdn.com/14593222096744.mp4
         * duration : 605
         * webUrl : {"raw":"http://www.wandoujia.com/eyepetizer/detail.html?vid=6316","forWeibo":"http://wandou.im/1t4uh4"}
         * releaseTime : 1461470400000
         * playInfo : [{"height":360,"width":640,"name":"流畅","type":"low","url":"http://baobab.wdjcdn.com/1459397364253_6316_640x360.mp4"},{"height":480,"width":854,"name":"标清","type":"normal","url":"http://baobab.wdjcdn.com/1459397020727_6316_854x480.mp4"},{"height":720,"width":1280,"name":"高清","type":"high","url":"http://baobab.wdjcdn.com/14593222096744.mp4"}]
         * consumption : {"collectionCount":252,"shareCount":135,"playCount":34902,"replyCount":25}
         * campaign : null
         * waterMarks : null
         * promotion : null
         * adTrack : null
         * idx : 0
         * shareAdTrack : null
         * favoriteAdTrack : null
         * webAdTrack : null
         * date : 1461470400000
         */

        private DataBean data;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public DataBean getData() {
            return data;
        }

        public void setData(DataBean data) {
            this.data = data;
        }

        public static class DataBean{
            private int id;
            private String title;
            private String description;
            /**
             * name : 优酷
             * alias : youku
             * icon : http://img.wdjimg.com/mms/icon/v1/3/2d/dc14dd1e40b8e561eae91584432262d3_256_256.png
             */

            private ProviderBean provider;
            private String category;
            private Object author;
            /**
             * feed : http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg
             * detail : http://img.wdjimg.com/image/video/1701cafbab106b129cf7a5dac4eadbc1_0_0.jpeg
             * blurred : http://img.wdjimg.com/image/video/c82852147bc84da735c566ae5000a76b_0_0.jpeg
             * sharing : null
             */

            private CoverBean cover;
            private String playUrl;
            private int duration;
            /**
             * raw : http://www.wandoujia.com/eyepetizer/detail.html?vid=6316
             * forWeibo : http://wandou.im/1t4uh4
             */

            private WebUrlBean webUrl;
            private long releaseTime;
            /**
             * collectionCount : 252
             * shareCount : 135
             * playCount : 34902
             * replyCount : 25
             */

            private ConsumptionBean consumption;
            private Object campaign;
            private Object waterMarks;
            private Object promotion;
            private Object adTrack;
            private int idx;
            private Object shareAdTrack;
            private Object favoriteAdTrack;
            private Object webAdTrack;
            private long date;
            /**
             * height : 360
             * width : 640
             * name : 流畅
             * type : low
             * url : http://baobab.wdjcdn.com/1459397364253_6316_640x360.mp4
             */

            private List<PlayInfoBean> playInfo;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public ProviderBean getProvider() {
                return provider;
            }

            public void setProvider(ProviderBean provider) {
                this.provider = provider;
            }

            public String getCategory() {
                return category;
            }

            public void setCategory(String category) {
                this.category = category;
            }

            public Object getAuthor() {
                return author;
            }

            public void setAuthor(Object author) {
                this.author = author;
            }

            public CoverBean getCover() {
                return cover;
            }

            public void setCover(CoverBean cover) {
                this.cover = cover;
            }

            public String getPlayUrl() {
                return playUrl;
            }

            public void setPlayUrl(String playUrl) {
                this.playUrl = playUrl;
            }

            public int getDuration() {
                return duration;
            }

            public void setDuration(int duration) {
                this.duration = duration;
            }

            public WebUrlBean getWebUrl() {
                return webUrl;
            }

            public void setWebUrl(WebUrlBean webUrl) {
                this.webUrl = webUrl;
            }

            public long getReleaseTime() {
                return releaseTime;
            }

            public void setReleaseTime(long releaseTime) {
                this.releaseTime = releaseTime;
            }

            public ConsumptionBean getConsumption() {
                return consumption;
            }

            public void setConsumption(ConsumptionBean consumption) {
                this.consumption = consumption;
            }

            public Object getCampaign() {
                return campaign;
            }

            public void setCampaign(Object campaign) {
                this.campaign = campaign;
            }

            public Object getWaterMarks() {
                return waterMarks;
            }

            public void setWaterMarks(Object waterMarks) {
                this.waterMarks = waterMarks;
            }

            public Object getPromotion() {
                return promotion;
            }

            public void setPromotion(Object promotion) {
                this.promotion = promotion;
            }

            public Object getAdTrack() {
                return adTrack;
            }

            public void setAdTrack(Object adTrack) {
                this.adTrack = adTrack;
            }

            public int getIdx() {
                return idx;
            }

            public void setIdx(int idx) {
                this.idx = idx;
            }

            public Object getShareAdTrack() {
                return shareAdTrack;
            }

            public void setShareAdTrack(Object shareAdTrack) {
                this.shareAdTrack = shareAdTrack;
            }

            public Object getFavoriteAdTrack() {
                return favoriteAdTrack;
            }

            public void setFavoriteAdTrack(Object favoriteAdTrack) {
                this.favoriteAdTrack = favoriteAdTrack;
            }

            public Object getWebAdTrack() {
                return webAdTrack;
            }

            public void setWebAdTrack(Object webAdTrack) {
                this.webAdTrack = webAdTrack;
            }

            public long getDate() {
                return date;
            }

            public void setDate(long date) {
                this.date = date;
            }

            public List<PlayInfoBean> getPlayInfo() {
                return playInfo;
            }

            public void setPlayInfo(List<PlayInfoBean> playInfo) {
                this.playInfo = playInfo;
            }

            public static class ProviderBean {
                private String name;
                private String alias;
                private String icon;

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getAlias() {
                    return alias;
                }

                public void setAlias(String alias) {
                    this.alias = alias;
                }

                public String getIcon() {
                    return icon;
                }

                public void setIcon(String icon) {
                    this.icon = icon;
                }
            }

            public static class CoverBean {
                private String feed;
                private String detail;
                private String blurred;
                private Object sharing;

                public String getFeed() {
                    return feed;
                }

                public void setFeed(String feed) {
                    this.feed = feed;
                }

                public String getDetail() {
                    return detail;
                }

                public void setDetail(String detail) {
                    this.detail = detail;
                }

                public String getBlurred() {
                    return blurred;
                }

                public void setBlurred(String blurred) {
                    this.blurred = blurred;
                }

                public Object getSharing() {
                    return sharing;
                }

                public void setSharing(Object sharing) {
                    this.sharing = sharing;
                }
            }

            public static class WebUrlBean {
                private String raw;
                private String forWeibo;

                public String getRaw() {
                    return raw;
                }

                public void setRaw(String raw) {
                    this.raw = raw;
                }

                public String getForWeibo() {
                    return forWeibo;
                }

                public void setForWeibo(String forWeibo) {
                    this.forWeibo = forWeibo;
                }
            }

            public static class ConsumptionBean {
                private int collectionCount;
                private int shareCount;
                private int playCount;
                private int replyCount;

                public int getCollectionCount() {
                    return collectionCount;
                }

                public void setCollectionCount(int collectionCount) {
                    this.collectionCount = collectionCount;
                }

                public int getShareCount() {
                    return shareCount;
                }

                public void setShareCount(int shareCount) {
                    this.shareCount = shareCount;
                }

                public int getPlayCount() {
                    return playCount;
                }

                public void setPlayCount(int playCount) {
                    this.playCount = playCount;
                }

                public int getReplyCount() {
                    return replyCount;
                }

                public void setReplyCount(int replyCount) {
                    this.replyCount = replyCount;
                }
            }

            public static class PlayInfoBean {
                private int height;
                private int width;
                private String name;
                private String type;
                private String url;

                public int getHeight() {
                    return height;
                }

                public void setHeight(int height) {
                    this.height = height;
                }

                public int getWidth() {
                    return width;
                }

                public void setWidth(int width) {
                    this.width = width;
                }

                public String getName() {
                    return name;
                }

                public void setName(String name) {
                    this.name = name;
                }

                public String getType() {
                    return type;
                }

                public void setType(String type) {
                    this.type = type;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }
        }
    }
}
