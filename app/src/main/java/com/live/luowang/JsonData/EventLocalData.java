package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/13.
 */
public class EventLocalData {

    /**
     * err_code : 0
     * msg :
     * data : {"items":[{"place_id":100209,"name":"小灯塔咖啡馆","name_en":"xiaodengtakafeiguan","logo_url":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/livehouse/100209/MTAwMjA5.jpg","distance":"8.525195","province_id":"101396","city_id":"101443","city_name":"烟台","fav":"9","comm":"0","type_id":"1","type_name":"Livehouse","lng":"121.409934","lat":"37.543926"},{"place_id":100108,"name":"烟台哈瓦那酒吧","name_en":"hawanabar","logo_url":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/livehouse/100108/MTAwMTA4.jpg","distance":"9.133968","province_id":"101396","city_id":"101443","city_name":"烟台","fav":"9","comm":"0","type_id":"1","type_name":"Livehouse","lng":"121.408178","lat":"37.549285"},{"place_id":100508,"name":"梦工厂 Livehouse","name_en":"Dreamill","logo_url":"http://s.luoo.net/img/avatar.gif","distance":"55.017761","province_id":"101396","city_id":"101489","city_name":"威海","fav":"1","comm":"0","type_id":"1","type_name":"Livehouse","lng":"122.067722","lat":"37.531676"}],"pages":1}
     */

    private int err_code;
    private String msg;
    /**
     * items : [{"place_id":100209,"name":"小灯塔咖啡馆","name_en":"xiaodengtakafeiguan","logo_url":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/livehouse/100209/MTAwMjA5.jpg","distance":"8.525195","province_id":"101396","city_id":"101443","city_name":"烟台","fav":"9","comm":"0","type_id":"1","type_name":"Livehouse","lng":"121.409934","lat":"37.543926"},{"place_id":100108,"name":"烟台哈瓦那酒吧","name_en":"hawanabar","logo_url":"http://7xkszy.com2.z0.glb.qiniucdn.com/pics/livehouse/100108/MTAwMTA4.jpg","distance":"9.133968","province_id":"101396","city_id":"101443","city_name":"烟台","fav":"9","comm":"0","type_id":"1","type_name":"Livehouse","lng":"121.408178","lat":"37.549285"},{"place_id":100508,"name":"梦工厂 Livehouse","name_en":"Dreamill","logo_url":"http://s.luoo.net/img/avatar.gif","distance":"55.017761","province_id":"101396","city_id":"101489","city_name":"威海","fav":"1","comm":"0","type_id":"1","type_name":"Livehouse","lng":"122.067722","lat":"37.531676"}]
     * pages : 1
     */

    private DataBean data;

    public int getErr_code() {
        return err_code;
    }

    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private int pages;
        /**
         * place_id : 100209
         * name : 小灯塔咖啡馆
         * name_en : xiaodengtakafeiguan
         * logo_url : http://7xkszy.com2.z0.glb.qiniucdn.com/pics/livehouse/100209/MTAwMjA5.jpg
         * distance : 8.525195
         * province_id : 101396
         * city_id : 101443
         * city_name : 烟台
         * fav : 9
         * comm : 0
         * type_id : 1
         * type_name : Livehouse
         * lng : 121.409934
         * lat : 37.543926
         */

        private List<ItemsBean> items;

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            private int place_id;
            private String name;
            private String name_en;
            private String logo_url;
            private String distance;
            private String province_id;
            private String city_id;
            private String city_name;
            private String fav;
            private String comm;
            private String type_id;
            private String type_name;
            private String lng;
            private String lat;

            public int getPlace_id() {
                return place_id;
            }

            public void setPlace_id(int place_id) {
                this.place_id = place_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getName_en() {
                return name_en;
            }

            public void setName_en(String name_en) {
                this.name_en = name_en;
            }

            public String getLogo_url() {
                return logo_url;
            }

            public void setLogo_url(String logo_url) {
                this.logo_url = logo_url;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public String getProvince_id() {
                return province_id;
            }

            public void setProvince_id(String province_id) {
                this.province_id = province_id;
            }

            public String getCity_id() {
                return city_id;
            }

            public void setCity_id(String city_id) {
                this.city_id = city_id;
            }

            public String getCity_name() {
                return city_name;
            }

            public void setCity_name(String city_name) {
                this.city_name = city_name;
            }

            public String getFav() {
                return fav;
            }

            public void setFav(String fav) {
                this.fav = fav;
            }

            public String getComm() {
                return comm;
            }

            public void setComm(String comm) {
                this.comm = comm;
            }

            public String getType_id() {
                return type_id;
            }

            public void setType_id(String type_id) {
                this.type_id = type_id;
            }

            public String getType_name() {
                return type_name;
            }

            public void setType_name(String type_name) {
                this.type_name = type_name;
            }

            public String getLng() {
                return lng;
            }

            public void setLng(String lng) {
                this.lng = lng;
            }

            public String getLat() {
                return lat;
            }

            public void setLat(String lat) {
                this.lat = lat;
            }
        }
    }
}
