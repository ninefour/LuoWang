package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/13.
 */
public class ForumBannerData {

    /**
     * err_code : 0
     * msg :
     * data : {"items":[{"ad_id":"302","image":"http://7xkszy.com2.z0.glb.qiniucdn.com/site/201604/5704c725a649c.jpg","title":"恋空","has_comment":"yes","has_music":"yes","type":"9","res_value":"571","comm":0,"forum_name":"恋空"},{"ad_id":"303","image":"http://7xkszy.com2.z0.glb.qiniucdn.com/site/201604/5704c771b2532.jpg","title":"电子火花","has_comment":"yes","has_music":"yes","type":"9","res_value":"572","comm":0,"forum_name":"电子火花"},{"ad_id":"306","image":"http://7xkszy.com2.z0.glb.qiniucdn.com/site/201604/570b6262713b0.jpg","title":"治愈的频率","has_comment":"yes","has_music":"yes","type":"9","res_value":"111","comm":0,"forum_name":"后来摇啊摇"}]}
     */

    private int err_code;
    private String msg;
    private DataBean data;

    public int getErr_code() {
        return err_code;
    }

    public void setErr_code(int err_code) {
        this.err_code = err_code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * ad_id : 302
         * image : http://7xkszy.com2.z0.glb.qiniucdn.com/site/201604/5704c725a649c.jpg
         * title : 恋空
         * has_comment : yes
         * has_music : yes
         * type : 9
         * res_value : 571
         * comm : 0
         * forum_name : 恋空
         */

        private List<ItemsBean> items;

        public List<ItemsBean> getItems() {
            return items;
        }

        public void setItems(List<ItemsBean> items) {
            this.items = items;
        }

        public static class ItemsBean {
            private String ad_id;
            private String image;
            private String title;
            private String has_comment;
            private String has_music;
            private String type;
            private String res_value;
            private int comm;
            private String forum_name;

            public String getAd_id() {
                return ad_id;
            }

            public void setAd_id(String ad_id) {
                this.ad_id = ad_id;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getHas_comment() {
                return has_comment;
            }

            public void setHas_comment(String has_comment) {
                this.has_comment = has_comment;
            }

            public String getHas_music() {
                return has_music;
            }

            public void setHas_music(String has_music) {
                this.has_music = has_music;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }

            public String getRes_value() {
                return res_value;
            }

            public void setRes_value(String res_value) {
                this.res_value = res_value;
            }

            public int getComm() {
                return comm;
            }

            public void setComm(int comm) {
                this.comm = comm;
            }

            public String getForum_name() {
                return forum_name;
            }

            public void setForum_name(String forum_name) {
                this.forum_name = forum_name;
            }
        }
    }
}
