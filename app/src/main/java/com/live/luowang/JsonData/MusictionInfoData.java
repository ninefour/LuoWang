package com.live.luowang.JsonData;

import java.util.List;

/**
 * Created by Chen on 2016/4/20.
 */
public class MusictionInfoData {

    /**
     * id : 995957322
     * uid : 543
     * bsname :
     * title : 侯康：有温度的声音
     * category_id : 543
     * group_id : 0
     * description : 在音乐中找一片净土，让心扎根长出自由。
     * root : 0
     * pid : 0
     * model_id : 0
     * type : 2
     * position : 0
     * link_id : 0
     * cover_url : http://cdn.wawa.fm/group1/M00/02/4E/Cvtf3VcUmz6AWBeSAADE6rOmSRI291.jpg
     * thumbnail_url : http://cdn.wawa.fm/group1/M00/02/4E/Cvtf3VcUmz6AWBeSAADE6rOmSRI291_150X150.jpg
     * display : 1
     * deadline : 0
     * attach : 0
     * view : 0
     * comment : 0
     * extend : 0
     * level : 0
     * create_time : 1460949460
     * update_time : 1460949460
     * status : 1
     * nshow : 0
     * zits : 0
     * sits : 0
     * tempstatus : 0
     * publish_time : 1460963209
     * author_id : null
     * track : {"id":"21546","resourcecode":"20160331052705478","songer":"侯康","songname":"明天是否还能见到你","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89IeAF6N7AEwdGg4J8zM919.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:27:05","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89H2AQelZAHJC9G2sZTs015.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87TKAbu85AL7Yb79CE64232.mp3","time":"311000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265_150X150.jpg","fsize":"4988186","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87SSAdbBIAAABWiW_DeY634.txt","status":"1","flag":"0","note":null,"create_time":"1459416425","update_time":null,"publish_time":"1459416425","vol_id":null,"musician_id":"995957322","banner_id":null,"fee_tag":null,"play_count":"1643"}
     * other_tracks : [{"id":"21549","resourcecode":"20160331053007478","songer":"侯康","songname":"我是一个任性的孩子","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89CeAPaAuADnGmIi2n4A648.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87gCAMScCAAMzaE3Uobs342.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:30:07","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89CCAGpZBAFa7kPT0rPI678.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87heAd4GyAJDlK1DS8So175.mp3","time":"236000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87gCAMScCAAMzaE3Uobs342_150X150.jpg","fsize":"3786392","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87g2ADyliAAACAOQqQRo303.txt","status":"1","flag":"0","note":null,"create_time":"1459416607","update_time":null,"publish_time":"1459416607","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"281"},{"id":"21548","resourcecode":"20160331052922478","songer":"侯康","songname":"亲爱的姑娘","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89EeAMcksADp9CFqGIS0392.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87bOAVgrMAAMzaE3Uobs176.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:29:22","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89DyAAcynAFfNcIkEcYU616.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87dKAF3VZAJKO6eAEgkg051.mp3","time":"239000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87bOAVgrMAAMzaE3Uobs176_150X150.jpg","fsize":"3833096","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87cWAbvnRAAABTnsPF0g454.txt","status":"1","flag":"0","note":null,"create_time":"1459416562","update_time":null,"publish_time":"1459416562","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"297"},{"id":"21547","resourcecode":"20160331052802478","songer":"侯康","songname":"鸟与人","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89GeAHmOwAE4xwbycSrw605.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87XiAQ0dVAAMzaE3Uobs104.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:28:02","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89F6AWe28AHVikok9NUk715.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87ZeAfmCpAMQPIbgUiO8149.mp3","time":"320000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87XiAQ0dVAAMzaE3Uobs104_150X150.jpg","fsize":"5124545","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87YiADj_yAAABytZk7nI502.txt","status":"1","flag":"0","note":null,"create_time":"1459416482","update_time":null,"publish_time":"1459416482","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"332"},{"id":"21546","resourcecode":"20160331052705478","songer":"侯康","songname":"明天是否还能见到你","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89IeAF6N7AEwdGg4J8zM919.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:27:05","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89H2AQelZAHJC9G2sZTs015.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87TKAbu85AL7Yb79CE64232.mp3","time":"311000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265_150X150.jpg","fsize":"4988186","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87SSAdbBIAAABWiW_DeY634.txt","status":"1","flag":"0","note":null,"create_time":"1459416425","update_time":null,"publish_time":"1459416425","vol_id":null,"musician_id":"995957322","banner_id":null,"fee_tag":null,"play_count":"1643"},{"id":"21545","resourcecode":"20160331052525478","songer":"侯康","songname":"忽如其来的风","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89KmAXtFZAFtqn7lx8ps174.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87MeAbVMxAAMzaE3Uobs330.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:25:25","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89J-AThHZAIk77lvLdQ8844.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87OqAH5nyAOUww9DY_ek034.mp3","time":"374000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87MeAbVMxAAMzaE3Uobs330_150X150.jpg","fsize":"5991071","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87NiAfoNtAAABtg5OSh0078.txt","status":"1","flag":"0","note":null,"create_time":"1459416325","update_time":null,"publish_time":"1459416325","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"407"},{"id":"21544","resourcecode":"20160331052408478","songer":"侯康","songname":"跟我走吧","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89MKAeekBADZSGd0EDN4006.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87IyAZhqeAAMzaE3Uobs349.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 17:24:08","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89LuAUsCbAFGLwuyDMng458.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87KOAPvbaAIg82Fs3nEc709.mp3","time":"222000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87IyAZhqeAAMzaE3Uobs349_150X150.jpg","fsize":"3559961","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87JuAFExIAAABxhjairQ536.txt","status":"1","flag":"0","note":null,"create_time":"1459416248","update_time":null,"publish_time":"1459416248","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"1070"},{"id":"21543","resourcecode":"20160331045149478","songer":"侯康","songname":"睡吧宝贝","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb86YKAQ9WVAC22MHj85e0573.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85QaAUqbgAAMzaE3Uobs441.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 16:51:49","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb86XuAZ_KMAESfQAlV3ys255.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85RuAESggAHKK_seB-QI379.mp3","time":"187000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85QaAUqbgAAMzaE3Uobs441_150X150.jpg","fsize":"2995760","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85RWAXLHXAAABLr2KyH8710.txt","status":"1","flag":"0","note":null,"create_time":"1459414309","update_time":null,"publish_time":"1459414309","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"208"},{"id":"21542","resourcecode":"20160331045026478","songer":"侯康","songname":"赶在七月之前","songalbum":null,"filename":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb86Z2AMFEjAEGYhfpF8cE555.mp3","songphoto":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85HWACy8fAAMzaE3Uobs160.jpg","remarks":null,"del_flag":"0","create_by":"3","create_date":"2016-03-31 16:50:26","update_by":"3","update_date":null,"filename192":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb86ZWAYxEOAGJ42lnFCE8775.mp3","filename320":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85KeAJaSEAKR9fykBRI8360.mp3","time":"268000","thumbnail_url":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85HWACy8fAAMzaE3Uobs160_150X150.jpg","fsize":"4298885","ggid":null,"ypid":null,"mtype":"1","uid":"478","lyrics":"http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb85LSAIZp4AAABXlx0qrs701.txt","status":"1","flag":"0","note":null,"create_time":"1459414226","update_time":null,"publish_time":"1459414226","vol_id":null,"musician_id":null,"banner_id":null,"fee_tag":null,"play_count":"496"}]
     * author : {"uid":"543","nickname":"乐人无数","password":null,"sex":"0","birthday":"2014-12-30","qq":"","score":"810","login":"568","reg_ip":"3232286656","reg_time":"1430195493","last_login_ip":"2005124983","last_login_time":"1456714537","status":"1","pimg":"http://wawa.fm/img/yueren_head.jpg","remark":"阅人有术，乐人无数！","card":"http://cdn.wawa.fm/group1/M00/00/21/Cvtf3VVS3OiAcK5gAAI_AdDaSac613.jpg","lanmu":"543","email":"276111403@qq.com","fsnum":"1","bqname":"","artnum":"94","type":"0","weibo":null,"wechat":null,"app_ilink":null,"app_alink":null,"website":null,"note":null,"use_tag":"0","subcription":"none"}
     * reprint_author : null
     * article : {"id":"995957322","content":"<p style=\"text-align: center;\">如果你拥有理想与爱情，<\/p><p style=\"text-align: center;\">希望你的理想与爱情充满阳光。<\/p><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUUZyAb-F3AABOITLmG3I370.png\" /><\/p><p>无论你是想要传播还是获得积极向上的力量，方式有很多种。并不是只有鸡汤文才能励志，反鸡汤虽然会给人当头一棒猛然一击，但是一针见血更能发人深省。并不是只能酣畅淋漓的摇滚音乐才能让我们热血沸腾，民谣何尝不可。<\/p><p>当然，这不代表民谣是站在摇滚的对立面，相反，我倒觉得两者之间存在着某种微妙的联系。走向民谣这条路上的人似乎跟摇滚都会有些许瓜葛，一个摇滚梦，亦或是一颗摇滚的心，也许正是青春年少时有过这样一个梦想，而后走向独立民谣之路时仍不忘初心。<\/p><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4E/Cvtf3VcUm1GANQIgAACeP3Tzhs4239.jpg\" /><\/p><p>侯康，北京城北的一名独立音乐人，音乐算是闲余时间的爱好。工作数载，仍旧保持着对音乐的热情。<\/p><p>印象里，他是个极亲切的音乐人，当我把访谈问题整理好，命名为&ldquo;中文笔试现在开始&rdquo;发过去之后，很快便收到了&ldquo;考试结束&rdquo;的文档，现在想起来仍觉得有趣儿。<\/p><p>&nbsp;<\/p><hr /><h3><span style=\"color:#A9A9A9;\">( 注：下文挖哇标注为W，侯康标注为H&nbsp;)<\/span><\/h3><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：您曾提到自己青春时也怀有摇滚梦，那个梦想您还记得是什么模样吗？然而为什么选择踏上一人一琴的民谣路？<\/span><\/p><p>H：很多男生在少年时期都会有个摇滚梦，我也一样，摇滚带来的那种直接、痛快、肾上腺素爆发的感觉非常过瘾，给过剩的精力找到了一个出口，把对现状的不满和对未来的不安全部释放出来，也给了躁动的心一个载体。<\/p><p>民谣相对舒缓、踏实、更有韧劲，其实也一直在我的&ldquo;歌单&rdquo;里，但从创作上说的话，我觉得需要沉下来，是一种生活的积淀，现在更偏向民谣。<\/p><p>摇滚和民谣对我来说都意味着&ldquo;真&rdquo;，只是形式不同，摇滚更侧重情绪，有反叛精神和批判性，肆意张扬。民谣更像是娓娓道来，诉说内心，映射世间百态，承接地气。两种都很棒，年龄越大会更喜欢沉静的表达吧，老了...<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：在您的豆瓣小站上除了民谣有很多纯音乐，在您看来，创作纯音乐和民谣歌曲有什么相同和不同之处？您对民谣的理解和认识？<\/span><\/p><p>H：纯音乐其实更加&ldquo;清澈&rdquo;，没有了文字这种具象的表达，完全靠你的想象力，我觉得反而更接近音乐的本质。<\/p><p>关于民谣，开始我就没打算写词，感觉自己也不善文笔，想过找人一起合作，但是没有人能完全表达出你的内心，你也很难把握别人的文字，到最后还是硬着头皮全自己来，不过慢慢也算找到了自己的方式，我写词的方式就像在跟人对话或倾诉，尽量做到真实。<\/p><h3 style=\"text-align: center;\"><span style=\"color:#A9A9A9;\">（ps：上面回答了一部分对民谣的理解和认识）<\/span><\/h3><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVHaAIl3hAABkSXjmQPw942.png\" /><\/p><p><span style=\"color:#808080;\">W：林夕坚持先有曲再有词，他喜欢用旋律来挑拨自己的情感。那您的创作习惯是怎样的？<\/span><\/p><p>H：我自己比较满意的作品大多都是词曲一起出来的，之后再慢慢调整，相对确实曲会比词早一些。<\/p><p style=\"text-align: center;\">&nbsp;<\/p><p><span style=\"color:#808080;\">W：《我是一个任性的孩子》这首作品的歌词选自顾城的诗，平时生活中您喜欢研究文学吗？除了顾城，您还喜欢哪些作家、诗人的文学作品呢？<\/span><\/p><p>H：我看书算不上多，文学类只是其中一部分，对我来说书籍就是&ldquo;老师&rdquo;。我觉得每个作者、诗人都有他的特点，作品都有吸引你的地方，很难只是挑出&ldquo;几个&rdquo;。<\/p><p>某一时段时间看了那些作品，可能就会映射到你生活中，对你产生影响。但是&ldquo;老师&rdquo;实在太多了，硬选出来，剩下的可能会生气&hellip;哈哈！<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：《鸟与人》这首歌很有深意，当时是在什么状态下创作了这首歌？其中有什么故事吗？<\/span><\/p><p>H：保密&hellip;开玩笑的，在众乐纪里有一篇文章专门写了一些创作灵感和动机，有兴趣的朋友可以去看看。<a href=\"http://mp.weixin.qq.com/s?__biz=MzA5NDQ4MTA3NQ==&amp;mid=403331353&amp;idx=1&amp;sn=831fc33a2a020ae8e13b9832f5f6d946&amp;scene=0#wechat_redirect\"><span style=\"color:#A9A9A9;\">（点此处查看文章）<\/span><\/a><\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：听您的音乐作品，无论是快歌还是慢歌，似乎都有一丝治愈的感觉，存在着一种积极的态度，这是您本质上想传达给听众的感觉吗？<\/span><\/p><p>H：确实是这样的，我有时候写歌写到后来会不自觉的留希望，往光明的那面跑，也许是潜意识吧，看来我还是个很&ldquo;nice&rdquo;的人。<\/p><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVIiAef5hABCqdlan5fQ794.jpg\" /><\/p><p><span style=\"color:#808080;\">W：不管是主流还是小众的音乐人，创作出的作品都会带有自己的特色，提高辨识度。您觉得自己的特别之处在哪？<\/span><\/p><p>H：这个问题不应该问我，特色、特点是别人给的，我也只能透过听众这面镜子才能了解自己。如果非要让我自己说，我希望我自己的特点是&ldquo;真&rdquo;。<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：您怎么理解&ldquo;小众&rdquo;和&ldquo;独立&rdquo;这两个词？<\/span><\/p><p>H：其实过去的很多小众音乐也都变成了今天的流行音乐，坚持小众和独立音乐的人相对都是有突破精神的，想去创造而不是模仿，敢去为音乐注入新的血液。<\/p><p>大家已经开始厌倦模块化、商业化的音乐模式，希望听到更多真实的、不一样的声音，我相信，未来独立音乐这股力量肯定会崛起，其中的一些也许就是之后的主流。<\/p><p>不过&ldquo;小众&rdquo;和&ldquo;独立&rdquo;这些标语很容易误导听众，有些极端的听众会觉得只有小众最牛，为了&ldquo;小众&rdquo;而&ldquo;小众&rdquo;，反而瞧不上其它音乐类型，这种情绪不好。我觉得只要用心做的音乐，流行也好，小众、独立也好&rdquo;，只要能打动我，我都会听。<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：音乐对您的影响？<\/span><\/p><p>H：其实我过去很长一段时间都是徘徊在音乐的门口，总感觉一下扎进去，会对身边的人有些不负责任，似乎除了歌火、人火，才能用音乐本身去证明你自己，才能获得更多生存的权利。<\/p><p>所以一直很矛盾，也曾经想过放弃，做一些所谓&ldquo;该做的事&rdquo;，但是到最后自己骗不了自己，只要一闲下来就会拿起吉他弹两下，时间长了不摸都难受，有时候弹着唱着都能哭出来...心甘情愿把很多精力&ldquo;浪费&rdquo;在音乐上。<\/p><p>在坚持音乐的过程中，也确实收获了不少，除了一些感悟，还给自己创造了一片净土，如果没有音乐这些都不会存在。<\/p><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVKiAEu0DAAIj6McIszs517.png\" /><\/p><p><span style=\"color:#808080;\">W：您是土生土长的北京人儿吗？北京给您的印象是什么呢？有很多拥有音乐梦想的人在北漂，您想对他们说些什么？<\/span><\/p><p>H：祖上不是，不过我是从小在这长大的，对我来说这就是我的故乡。对北京算是又爱又恨吧，感感觉现在的北京不太适合生活，拥堵、污染、浮躁，不过也没办法，毕竟这集中了太多资源，也承载了太多人的梦，尽管大部分都做了炮灰。<\/p><p>对于北漂做音乐的人，我觉得挺不容易的，能坚持着就已经算&ldquo;真爱&rdquo;了，更别说拿音乐糊口、挣钱。<\/p><p>不光是对北漂的音乐人吧，对所有喜欢音乐的人，我觉得都不应该过于极端，也别太清高，毕竟优秀的人太多了，先保生存，再谈理想，有很多方式可以平衡音乐与现实，但如果只是孤注一掷想出名、想发财，那就很容易陷进去，反而把最重要的东西丢了。<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：目前为止您的作品都是以单曲的形式发布，那第一张专辑会在什么时候发布呢？<\/span><\/p><p>H：目前还没有制作专辑，都是一首一首的单曲。专辑等准备好了就会发，时间不好说啊。<\/p><p>&nbsp;<\/p><p><span style=\"color:#808080;\">W：对于未来的音乐之路，您有什么憧憬？写一段话给十年后的自己吧！<\/span><\/p><p>H：对于音乐，既然已经放不下了，就坚持到底！希望十年后，自己能更加从容，风轻云淡，还在为喜欢音乐的人带来更多真诚的声音。<\/p><p style=\"text-align: center;\"><img alt=\"\" src=\"http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVMaASaOSAAkqoSAdnPk761.png\" /><\/p><p>生活中的侯康时常幽默，会用猴年吉祥物&ldquo;康康&rdquo;打趣自己：我这是要火嘛？偶尔也会失落：看着窗外密集穿梭的车流和面无表情默默奔走的人们，突然有一种莫名的失落感 ，觉得自己很世俗，却又无法挣脱这种自以为不需要的粉饰。更没想到他还有害羞的时候：不露正脸是我一贯的风格...&nbsp;&nbsp; &nbsp;<\/p><p>当我问及侯康的歌儿听来是怎样一种感受时，吕不三是这样形容的：就像是在一个非常静的冬夜，外面飘着雪，家里烧着一壶黄酒。自己一边看着雪，一边喝着酒...<\/p><h3>&nbsp;<\/h3><h3 style=\"text-align: justify;\"><span style=\"color:#808080;\">跟我走吧，不用再想太多。跟我走吧，扔掉那些过剩的忧伤。<\/span><\/h3><h3 style=\"text-align: justify;\">&nbsp;<\/h3><h3 style=\"text-align: justify;\"><span style=\"color:#808080;\">麻木的人们，散播着腐烂的思想。不要让他们，剥夺了你的青春。<\/span><\/h3><h3 style=\"text-align: justify;\">&nbsp;<\/h3><h3 style=\"text-align: justify;\"><span style=\"color:#808080;\">睁开沉睡的眼，去看看这世界。生命依然鲜艳，太阳照常升起。<\/span><\/h3><h3 style=\"text-align: justify;\">&nbsp;<\/h3><h3 style=\"text-align: justify;\"><span style=\"color:#808080;\">迈开沉重的脚，去天边转一转。再没谁能阻挡你，跟我来吧！<\/span><\/h3><h3 style=\"text-align: justify;\">&nbsp;<\/h3><h3 style=\"text-align: right;\"><span style=\"color:#808080;\">&mdash;&mdash;《跟我走吧》<\/span><\/h3><h3>&nbsp;<\/h3><p>侯康的音乐作品中，也会写有生活失落的一面，但这并不是全部。仔细品味你会发现其中总存有一抹希望，能够在寒冷冬夜中带给你一丝暖意，牵引着你从阴暗走向阳光，你是否体会到了呢？<\/p><h3><span style=\"color:#A9A9A9;\">( 特别叮嘱：如果哪位小伙伴去看侯康老师的演出现场，务必多拍点正脸皂呀呀呀！&nbsp;)<\/span><\/h3><p>&nbsp;<\/p><p style=\"text-align: center;\">最后，特别感谢侯康～～～<\/p>","musiccode":"20160331052705478","adimg":"","adlink":"","codepic":"","auid":"0","reason":"","audit_time":"0","yrid":"0"}
     * category : {"id":"543","name":"yueren","title":"乐人"}
     * statistics : {"view_count":"5981","comment_count":"4"}
     * recommend : [{"id":"993956575","uid":"543","title":"谢帝：我就是个成都娃娃","description":"中国各地总有这么一些人坚持用家乡话创作音乐。","cover_url":"http://cdn.wawa.fm/group1/M00/00/6B/Cvtf3VXByO6AMdJXAADRL3y7MUo867.jpg","track_id":"6520","view_count":"71712","play_count":"7532"},{"id":"993956650","uid":"543","title":"珂澜：卧室音乐人","description":"其实做交通规划和写歌差不多。","cover_url":"http://cdn.wawa.fm/group1/M00/00/91/Cvtf3VXv_xWAcNbbAACxeVDap68059.jpg","track_id":"9360","view_count":"59749","play_count":"16103"},{"id":"993956803","uid":"543","title":"TUX：蓝色阳光下","description":"糖衣里包装着悲伤","cover_url":"http://cdn.wawa.fm/group1/M00/01/0C/Cvtf3VZS3PaAKRZ5AADuXJP6f44829.jpg","track_id":"20974","view_count":"61412","play_count":"8919"},{"id":"914","uid":"660","title":"陈粒：\u201c你国唯一老公\u201d","description":"在这个处处需要\u201c抱大腿\u201d的时代，即便头顶\u201c独立音乐人\u201d的盛名，恐怕敢放出此话的歌手也是凤毛麟角；其中艰苦，知者自知。","cover_url":"http://cdn.wawa.fm/group1/M00/00/6C/Cvtf3VXB3nmAKk5WAADtNC3NTMM162.jpg","track_id":"6238","view_count":"16988","play_count":"5673"}]
     */

    private String id;
    private String uid;
    private String bsname;
    private String title;
    private String category_id;
    private String group_id;
    private String description;
    private String root;
    private String pid;
    private String model_id;
    private String type;
    private String position;
    private String link_id;
    private String cover_url;
    private String thumbnail_url;
    private String display;
    private String deadline;
    private String attach;
    private String view;
    private String comment;
    private String extend;
    private String level;
    private String create_time;
    private String update_time;
    private String status;
    private String nshow;
    private String zits;
    private String sits;
    private String tempstatus;
    private String publish_time;
    private Object author_id;
    /**
     * id : 21546
     * resourcecode : 20160331052705478
     * songer : 侯康
     * songname : 明天是否还能见到你
     * songalbum : null
     * filename : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89IeAF6N7AEwdGg4J8zM919.mp3
     * songphoto : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265.jpg
     * remarks : null
     * del_flag : 0
     * create_by : 3
     * create_date : 2016-03-31 17:27:05
     * update_by : 3
     * update_date : null
     * filename192 : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb89H2AQelZAHJC9G2sZTs015.mp3
     * filename320 : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87TKAbu85AL7Yb79CE64232.mp3
     * time : 311000
     * thumbnail_url : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87RaAWU2kAAMzaE3Uobs265_150X150.jpg
     * fsize : 4988186
     * ggid : null
     * ypid : null
     * mtype : 1
     * uid : 478
     * lyrics : http://cdn.wawa.fm/group1/M00/02/23/Cvtf3Vb87SSAdbBIAAABWiW_DeY634.txt
     * status : 1
     * flag : 0
     * note : null
     * create_time : 1459416425
     * update_time : null
     * publish_time : 1459416425
     * vol_id : null
     * musician_id : 995957322
     * banner_id : null
     * fee_tag : null
     * play_count : 1643
     */

    private TrackBean track;
    /**
     * uid : 543
     * nickname : 乐人无数
     * password : null
     * sex : 0
     * birthday : 2014-12-30
     * qq :
     * score : 810
     * login : 568
     * reg_ip : 3232286656
     * reg_time : 1430195493
     * last_login_ip : 2005124983
     * last_login_time : 1456714537
     * status : 1
     * pimg : http://wawa.fm/img/yueren_head.jpg
     * remark : 阅人有术，乐人无数！
     * card : http://cdn.wawa.fm/group1/M00/00/21/Cvtf3VVS3OiAcK5gAAI_AdDaSac613.jpg
     * lanmu : 543
     * email : 276111403@qq.com
     * fsnum : 1
     * bqname :
     * artnum : 94
     * type : 0
     * weibo : null
     * wechat : null
     * app_ilink : null
     * app_alink : null
     * website : null
     * note : null
     * use_tag : 0
     * subcription : none
     */

    private AuthorBean author;
    private Object reprint_author;
    /**
     * id : 995957322
     * content : <p style="text-align: center;">如果你拥有理想与爱情，</p><p style="text-align: center;">希望你的理想与爱情充满阳光。</p><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUUZyAb-F3AABOITLmG3I370.png" /></p><p>无论你是想要传播还是获得积极向上的力量，方式有很多种。并不是只有鸡汤文才能励志，反鸡汤虽然会给人当头一棒猛然一击，但是一针见血更能发人深省。并不是只能酣畅淋漓的摇滚音乐才能让我们热血沸腾，民谣何尝不可。</p><p>当然，这不代表民谣是站在摇滚的对立面，相反，我倒觉得两者之间存在着某种微妙的联系。走向民谣这条路上的人似乎跟摇滚都会有些许瓜葛，一个摇滚梦，亦或是一颗摇滚的心，也许正是青春年少时有过这样一个梦想，而后走向独立民谣之路时仍不忘初心。</p><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4E/Cvtf3VcUm1GANQIgAACeP3Tzhs4239.jpg" /></p><p>侯康，北京城北的一名独立音乐人，音乐算是闲余时间的爱好。工作数载，仍旧保持着对音乐的热情。</p><p>印象里，他是个极亲切的音乐人，当我把访谈问题整理好，命名为&ldquo;中文笔试现在开始&rdquo;发过去之后，很快便收到了&ldquo;考试结束&rdquo;的文档，现在想起来仍觉得有趣儿。</p><p>&nbsp;</p><hr /><h3><span style="color:#A9A9A9;">( 注：下文挖哇标注为W，侯康标注为H&nbsp;)</span></h3><p>&nbsp;</p><p><span style="color:#808080;">W：您曾提到自己青春时也怀有摇滚梦，那个梦想您还记得是什么模样吗？然而为什么选择踏上一人一琴的民谣路？</span></p><p>H：很多男生在少年时期都会有个摇滚梦，我也一样，摇滚带来的那种直接、痛快、肾上腺素爆发的感觉非常过瘾，给过剩的精力找到了一个出口，把对现状的不满和对未来的不安全部释放出来，也给了躁动的心一个载体。</p><p>民谣相对舒缓、踏实、更有韧劲，其实也一直在我的&ldquo;歌单&rdquo;里，但从创作上说的话，我觉得需要沉下来，是一种生活的积淀，现在更偏向民谣。</p><p>摇滚和民谣对我来说都意味着&ldquo;真&rdquo;，只是形式不同，摇滚更侧重情绪，有反叛精神和批判性，肆意张扬。民谣更像是娓娓道来，诉说内心，映射世间百态，承接地气。两种都很棒，年龄越大会更喜欢沉静的表达吧，老了...</p><p>&nbsp;</p><p><span style="color:#808080;">W：在您的豆瓣小站上除了民谣有很多纯音乐，在您看来，创作纯音乐和民谣歌曲有什么相同和不同之处？您对民谣的理解和认识？</span></p><p>H：纯音乐其实更加&ldquo;清澈&rdquo;，没有了文字这种具象的表达，完全靠你的想象力，我觉得反而更接近音乐的本质。</p><p>关于民谣，开始我就没打算写词，感觉自己也不善文笔，想过找人一起合作，但是没有人能完全表达出你的内心，你也很难把握别人的文字，到最后还是硬着头皮全自己来，不过慢慢也算找到了自己的方式，我写词的方式就像在跟人对话或倾诉，尽量做到真实。</p><h3 style="text-align: center;"><span style="color:#A9A9A9;">（ps：上面回答了一部分对民谣的理解和认识）</span></h3><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVHaAIl3hAABkSXjmQPw942.png" /></p><p><span style="color:#808080;">W：林夕坚持先有曲再有词，他喜欢用旋律来挑拨自己的情感。那您的创作习惯是怎样的？</span></p><p>H：我自己比较满意的作品大多都是词曲一起出来的，之后再慢慢调整，相对确实曲会比词早一些。</p><p style="text-align: center;">&nbsp;</p><p><span style="color:#808080;">W：《我是一个任性的孩子》这首作品的歌词选自顾城的诗，平时生活中您喜欢研究文学吗？除了顾城，您还喜欢哪些作家、诗人的文学作品呢？</span></p><p>H：我看书算不上多，文学类只是其中一部分，对我来说书籍就是&ldquo;老师&rdquo;。我觉得每个作者、诗人都有他的特点，作品都有吸引你的地方，很难只是挑出&ldquo;几个&rdquo;。</p><p>某一时段时间看了那些作品，可能就会映射到你生活中，对你产生影响。但是&ldquo;老师&rdquo;实在太多了，硬选出来，剩下的可能会生气&hellip;哈哈！</p><p>&nbsp;</p><p><span style="color:#808080;">W：《鸟与人》这首歌很有深意，当时是在什么状态下创作了这首歌？其中有什么故事吗？</span></p><p>H：保密&hellip;开玩笑的，在众乐纪里有一篇文章专门写了一些创作灵感和动机，有兴趣的朋友可以去看看。<a href="http://mp.weixin.qq.com/s?__biz=MzA5NDQ4MTA3NQ==&amp;mid=403331353&amp;idx=1&amp;sn=831fc33a2a020ae8e13b9832f5f6d946&amp;scene=0#wechat_redirect"><span style="color:#A9A9A9;">（点此处查看文章）</span></a></p><p>&nbsp;</p><p><span style="color:#808080;">W：听您的音乐作品，无论是快歌还是慢歌，似乎都有一丝治愈的感觉，存在着一种积极的态度，这是您本质上想传达给听众的感觉吗？</span></p><p>H：确实是这样的，我有时候写歌写到后来会不自觉的留希望，往光明的那面跑，也许是潜意识吧，看来我还是个很&ldquo;nice&rdquo;的人。</p><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVIiAef5hABCqdlan5fQ794.jpg" /></p><p><span style="color:#808080;">W：不管是主流还是小众的音乐人，创作出的作品都会带有自己的特色，提高辨识度。您觉得自己的特别之处在哪？</span></p><p>H：这个问题不应该问我，特色、特点是别人给的，我也只能透过听众这面镜子才能了解自己。如果非要让我自己说，我希望我自己的特点是&ldquo;真&rdquo;。</p><p>&nbsp;</p><p><span style="color:#808080;">W：您怎么理解&ldquo;小众&rdquo;和&ldquo;独立&rdquo;这两个词？</span></p><p>H：其实过去的很多小众音乐也都变成了今天的流行音乐，坚持小众和独立音乐的人相对都是有突破精神的，想去创造而不是模仿，敢去为音乐注入新的血液。</p><p>大家已经开始厌倦模块化、商业化的音乐模式，希望听到更多真实的、不一样的声音，我相信，未来独立音乐这股力量肯定会崛起，其中的一些也许就是之后的主流。</p><p>不过&ldquo;小众&rdquo;和&ldquo;独立&rdquo;这些标语很容易误导听众，有些极端的听众会觉得只有小众最牛，为了&ldquo;小众&rdquo;而&ldquo;小众&rdquo;，反而瞧不上其它音乐类型，这种情绪不好。我觉得只要用心做的音乐，流行也好，小众、独立也好&rdquo;，只要能打动我，我都会听。</p><p>&nbsp;</p><p><span style="color:#808080;">W：音乐对您的影响？</span></p><p>H：其实我过去很长一段时间都是徘徊在音乐的门口，总感觉一下扎进去，会对身边的人有些不负责任，似乎除了歌火、人火，才能用音乐本身去证明你自己，才能获得更多生存的权利。</p><p>所以一直很矛盾，也曾经想过放弃，做一些所谓&ldquo;该做的事&rdquo;，但是到最后自己骗不了自己，只要一闲下来就会拿起吉他弹两下，时间长了不摸都难受，有时候弹着唱着都能哭出来...心甘情愿把很多精力&ldquo;浪费&rdquo;在音乐上。</p><p>在坚持音乐的过程中，也确实收获了不少，除了一些感悟，还给自己创造了一片净土，如果没有音乐这些都不会存在。</p><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVKiAEu0DAAIj6McIszs517.png" /></p><p><span style="color:#808080;">W：您是土生土长的北京人儿吗？北京给您的印象是什么呢？有很多拥有音乐梦想的人在北漂，您想对他们说些什么？</span></p><p>H：祖上不是，不过我是从小在这长大的，对我来说这就是我的故乡。对北京算是又爱又恨吧，感感觉现在的北京不太适合生活，拥堵、污染、浮躁，不过也没办法，毕竟这集中了太多资源，也承载了太多人的梦，尽管大部分都做了炮灰。</p><p>对于北漂做音乐的人，我觉得挺不容易的，能坚持着就已经算&ldquo;真爱&rdquo;了，更别说拿音乐糊口、挣钱。</p><p>不光是对北漂的音乐人吧，对所有喜欢音乐的人，我觉得都不应该过于极端，也别太清高，毕竟优秀的人太多了，先保生存，再谈理想，有很多方式可以平衡音乐与现实，但如果只是孤注一掷想出名、想发财，那就很容易陷进去，反而把最重要的东西丢了。</p><p>&nbsp;</p><p><span style="color:#808080;">W：目前为止您的作品都是以单曲的形式发布，那第一张专辑会在什么时候发布呢？</span></p><p>H：目前还没有制作专辑，都是一首一首的单曲。专辑等准备好了就会发，时间不好说啊。</p><p>&nbsp;</p><p><span style="color:#808080;">W：对于未来的音乐之路，您有什么憧憬？写一段话给十年后的自己吧！</span></p><p>H：对于音乐，既然已经放不下了，就坚持到底！希望十年后，自己能更加从容，风轻云淡，还在为喜欢音乐的人带来更多真诚的声音。</p><p style="text-align: center;"><img alt="" src="http://files.wawa.fm:8888/group1/M00/02/4C/Cvtf3VcUVMaASaOSAAkqoSAdnPk761.png" /></p><p>生活中的侯康时常幽默，会用猴年吉祥物&ldquo;康康&rdquo;打趣自己：我这是要火嘛？偶尔也会失落：看着窗外密集穿梭的车流和面无表情默默奔走的人们，突然有一种莫名的失落感 ，觉得自己很世俗，却又无法挣脱这种自以为不需要的粉饰。更没想到他还有害羞的时候：不露正脸是我一贯的风格...&nbsp;&nbsp; &nbsp;</p><p>当我问及侯康的歌儿听来是怎样一种感受时，吕不三是这样形容的：就像是在一个非常静的冬夜，外面飘着雪，家里烧着一壶黄酒。自己一边看着雪，一边喝着酒...</p><h3>&nbsp;</h3><h3 style="text-align: justify;"><span style="color:#808080;">跟我走吧，不用再想太多。跟我走吧，扔掉那些过剩的忧伤。</span></h3><h3 style="text-align: justify;">&nbsp;</h3><h3 style="text-align: justify;"><span style="color:#808080;">麻木的人们，散播着腐烂的思想。不要让他们，剥夺了你的青春。</span></h3><h3 style="text-align: justify;">&nbsp;</h3><h3 style="text-align: justify;"><span style="color:#808080;">睁开沉睡的眼，去看看这世界。生命依然鲜艳，太阳照常升起。</span></h3><h3 style="text-align: justify;">&nbsp;</h3><h3 style="text-align: justify;"><span style="color:#808080;">迈开沉重的脚，去天边转一转。再没谁能阻挡你，跟我来吧！</span></h3><h3 style="text-align: justify;">&nbsp;</h3><h3 style="text-align: right;"><span style="color:#808080;">&mdash;&mdash;《跟我走吧》</span></h3><h3>&nbsp;</h3><p>侯康的音乐作品中，也会写有生活失落的一面，但这并不是全部。仔细品味你会发现其中总存有一抹希望，能够在寒冷冬夜中带给你一丝暖意，牵引着你从阴暗走向阳光，你是否体会到了呢？</p><h3><span style="color:#A9A9A9;">( 特别叮嘱：如果哪位小伙伴去看侯康老师的演出现场，务必多拍点正脸皂呀呀呀！&nbsp;)</span></h3><p>&nbsp;</p><p style="text-align: center;">最后，特别感谢侯康～～～</p>
     * musiccode : 20160331052705478
     * adimg :
     * adlink :
     * codepic :
     * auid : 0
     * reason :
     * audit_time : 0
     * yrid : 0
     */

    private ArticleBean article;
    /**
     * id : 543
     * name : yueren
     * title : 乐人
     */

    private CategoryBean category;
    /**
     * view_count : 5981
     * comment_count : 4
     */

    private StatisticsBean statistics;
    private List<TrackBean> other_tracks;
    /**
     * id : 993956575
     * uid : 543
     * title : 谢帝：我就是个成都娃娃
     * description : 中国各地总有这么一些人坚持用家乡话创作音乐。
     * cover_url : http://cdn.wawa.fm/group1/M00/00/6B/Cvtf3VXByO6AMdJXAADRL3y7MUo867.jpg
     * track_id : 6520
     * view_count : 71712
     * play_count : 7532
     */

    private List<RecommendBean> recommend;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getBsname() {
        return bsname;
    }

    public void setBsname(String bsname) {
        this.bsname = bsname;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getLink_id() {
        return link_id;
    }

    public void setLink_id(String link_id) {
        this.link_id = link_id;
    }

    public String getCover_url() {
        return cover_url;
    }

    public void setCover_url(String cover_url) {
        this.cover_url = cover_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public String getAttach() {
        return attach;
    }

    public void setAttach(String attach) {
        this.attach = attach;
    }

    public String getView() {
        return view;
    }

    public void setView(String view) {
        this.view = view;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(String extend) {
        this.extend = extend;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getCreate_time() {
        return create_time;
    }

    public void setCreate_time(String create_time) {
        this.create_time = create_time;
    }

    public String getUpdate_time() {
        return update_time;
    }

    public void setUpdate_time(String update_time) {
        this.update_time = update_time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getNshow() {
        return nshow;
    }

    public void setNshow(String nshow) {
        this.nshow = nshow;
    }

    public String getZits() {
        return zits;
    }

    public void setZits(String zits) {
        this.zits = zits;
    }

    public String getSits() {
        return sits;
    }

    public void setSits(String sits) {
        this.sits = sits;
    }

    public String getTempstatus() {
        return tempstatus;
    }

    public void setTempstatus(String tempstatus) {
        this.tempstatus = tempstatus;
    }

    public String getPublish_time() {
        return publish_time;
    }

    public void setPublish_time(String publish_time) {
        this.publish_time = publish_time;
    }

    public Object getAuthor_id() {
        return author_id;
    }

    public void setAuthor_id(Object author_id) {
        this.author_id = author_id;
    }

    public TrackBean getTrack() {
        return track;
    }

    public void setTrack(TrackBean track) {
        this.track = track;
    }

    public AuthorBean getAuthor() {
        return author;
    }

    public void setAuthor(AuthorBean author) {
        this.author = author;
    }

    public Object getReprint_author() {
        return reprint_author;
    }

    public void setReprint_author(Object reprint_author) {
        this.reprint_author = reprint_author;
    }

    public ArticleBean getArticle() {
        return article;
    }

    public void setArticle(ArticleBean article) {
        this.article = article;
    }

    public CategoryBean getCategory() {
        return category;
    }

    public void setCategory(CategoryBean category) {
        this.category = category;
    }

    public StatisticsBean getStatistics() {
        return statistics;
    }

    public void setStatistics(StatisticsBean statistics) {
        this.statistics = statistics;
    }

    public List<TrackBean> getOther_tracks() {
        return other_tracks;
    }

    public void setOther_tracks(List<TrackBean> other_tracks) {
        this.other_tracks = other_tracks;
    }

    public List<RecommendBean> getRecommend() {
        return recommend;
    }

    public void setRecommend(List<RecommendBean> recommend) {
        this.recommend = recommend;
    }

    public static class TrackBean {
        private String id;
        private String resourcecode;
        private String songer;
        private String songname;
        private Object songalbum;
        private String filename;
        private String songphoto;
        private Object remarks;
        private String del_flag;
        private String create_by;
        private String create_date;
        private String update_by;
        private Object update_date;
        private String filename192;
        private String filename320;
        private String time;
        private String thumbnail_url;
        private String fsize;
        private Object ggid;
        private Object ypid;
        private String mtype;
        private String uid;
        private String lyrics;
        private String status;
        private String flag;
        private Object note;
        private String create_time;
        private Object update_time;
        private String publish_time;
        private Object vol_id;
        private String musician_id;
        private Object banner_id;
        private Object fee_tag;
        private String play_count;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getResourcecode() {
            return resourcecode;
        }

        public void setResourcecode(String resourcecode) {
            this.resourcecode = resourcecode;
        }

        public String getSonger() {
            return songer;
        }

        public void setSonger(String songer) {
            this.songer = songer;
        }

        public String getSongname() {
            return songname;
        }

        public void setSongname(String songname) {
            this.songname = songname;
        }

        public Object getSongalbum() {
            return songalbum;
        }

        public void setSongalbum(Object songalbum) {
            this.songalbum = songalbum;
        }

        public String getFilename() {
            return filename;
        }

        public void setFilename(String filename) {
            this.filename = filename;
        }

        public String getSongphoto() {
            return songphoto;
        }

        public void setSongphoto(String songphoto) {
            this.songphoto = songphoto;
        }

        public Object getRemarks() {
            return remarks;
        }

        public void setRemarks(Object remarks) {
            this.remarks = remarks;
        }

        public String getDel_flag() {
            return del_flag;
        }

        public void setDel_flag(String del_flag) {
            this.del_flag = del_flag;
        }

        public String getCreate_by() {
            return create_by;
        }

        public void setCreate_by(String create_by) {
            this.create_by = create_by;
        }

        public String getCreate_date() {
            return create_date;
        }

        public void setCreate_date(String create_date) {
            this.create_date = create_date;
        }

        public String getUpdate_by() {
            return update_by;
        }

        public void setUpdate_by(String update_by) {
            this.update_by = update_by;
        }

        public Object getUpdate_date() {
            return update_date;
        }

        public void setUpdate_date(Object update_date) {
            this.update_date = update_date;
        }

        public String getFilename192() {
            return filename192;
        }

        public void setFilename192(String filename192) {
            this.filename192 = filename192;
        }

        public String getFilename320() {
            return filename320;
        }

        public void setFilename320(String filename320) {
            this.filename320 = filename320;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getThumbnail_url() {
            return thumbnail_url;
        }

        public void setThumbnail_url(String thumbnail_url) {
            this.thumbnail_url = thumbnail_url;
        }

        public String getFsize() {
            return fsize;
        }

        public void setFsize(String fsize) {
            this.fsize = fsize;
        }

        public Object getGgid() {
            return ggid;
        }

        public void setGgid(Object ggid) {
            this.ggid = ggid;
        }

        public Object getYpid() {
            return ypid;
        }

        public void setYpid(Object ypid) {
            this.ypid = ypid;
        }

        public String getMtype() {
            return mtype;
        }

        public void setMtype(String mtype) {
            this.mtype = mtype;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getLyrics() {
            return lyrics;
        }

        public void setLyrics(String lyrics) {
            this.lyrics = lyrics;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getFlag() {
            return flag;
        }

        public void setFlag(String flag) {
            this.flag = flag;
        }

        public Object getNote() {
            return note;
        }

        public void setNote(Object note) {
            this.note = note;
        }

        public String getCreate_time() {
            return create_time;
        }

        public void setCreate_time(String create_time) {
            this.create_time = create_time;
        }

        public Object getUpdate_time() {
            return update_time;
        }

        public void setUpdate_time(Object update_time) {
            this.update_time = update_time;
        }

        public String getPublish_time() {
            return publish_time;
        }

        public void setPublish_time(String publish_time) {
            this.publish_time = publish_time;
        }

        public Object getVol_id() {
            return vol_id;
        }

        public void setVol_id(Object vol_id) {
            this.vol_id = vol_id;
        }

        public String getMusician_id() {
            return musician_id;
        }

        public void setMusician_id(String musician_id) {
            this.musician_id = musician_id;
        }

        public Object getBanner_id() {
            return banner_id;
        }

        public void setBanner_id(Object banner_id) {
            this.banner_id = banner_id;
        }

        public Object getFee_tag() {
            return fee_tag;
        }

        public void setFee_tag(Object fee_tag) {
            this.fee_tag = fee_tag;
        }

        public String getPlay_count() {
            return play_count;
        }

        public void setPlay_count(String play_count) {
            this.play_count = play_count;
        }
    }

    public static class AuthorBean {
        private String uid;
        private String nickname;
        private Object password;
        private String sex;
        private String birthday;
        private String qq;
        private String score;
        private String login;
        private String reg_ip;
        private String reg_time;
        private String last_login_ip;
        private String last_login_time;
        private String status;
        private String pimg;
        private String remark;
        private String card;
        private String lanmu;
        private String email;
        private String fsnum;
        private String bqname;
        private String artnum;
        private String type;
        private Object weibo;
        private Object wechat;
        private Object app_ilink;
        private Object app_alink;
        private Object website;
        private Object note;
        private String use_tag;
        private String subcription;

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getNickname() {
            return nickname;
        }

        public void setNickname(String nickname) {
            this.nickname = nickname;
        }

        public Object getPassword() {
            return password;
        }

        public void setPassword(Object password) {
            this.password = password;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getBirthday() {
            return birthday;
        }

        public void setBirthday(String birthday) {
            this.birthday = birthday;
        }

        public String getQq() {
            return qq;
        }

        public void setQq(String qq) {
            this.qq = qq;
        }

        public String getScore() {
            return score;
        }

        public void setScore(String score) {
            this.score = score;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getReg_ip() {
            return reg_ip;
        }

        public void setReg_ip(String reg_ip) {
            this.reg_ip = reg_ip;
        }

        public String getReg_time() {
            return reg_time;
        }

        public void setReg_time(String reg_time) {
            this.reg_time = reg_time;
        }

        public String getLast_login_ip() {
            return last_login_ip;
        }

        public void setLast_login_ip(String last_login_ip) {
            this.last_login_ip = last_login_ip;
        }

        public String getLast_login_time() {
            return last_login_time;
        }

        public void setLast_login_time(String last_login_time) {
            this.last_login_time = last_login_time;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPimg() {
            return pimg;
        }

        public void setPimg(String pimg) {
            this.pimg = pimg;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getCard() {
            return card;
        }

        public void setCard(String card) {
            this.card = card;
        }

        public String getLanmu() {
            return lanmu;
        }

        public void setLanmu(String lanmu) {
            this.lanmu = lanmu;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getFsnum() {
            return fsnum;
        }

        public void setFsnum(String fsnum) {
            this.fsnum = fsnum;
        }

        public String getBqname() {
            return bqname;
        }

        public void setBqname(String bqname) {
            this.bqname = bqname;
        }

        public String getArtnum() {
            return artnum;
        }

        public void setArtnum(String artnum) {
            this.artnum = artnum;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public Object getWeibo() {
            return weibo;
        }

        public void setWeibo(Object weibo) {
            this.weibo = weibo;
        }

        public Object getWechat() {
            return wechat;
        }

        public void setWechat(Object wechat) {
            this.wechat = wechat;
        }

        public Object getApp_ilink() {
            return app_ilink;
        }

        public void setApp_ilink(Object app_ilink) {
            this.app_ilink = app_ilink;
        }

        public Object getApp_alink() {
            return app_alink;
        }

        public void setApp_alink(Object app_alink) {
            this.app_alink = app_alink;
        }

        public Object getWebsite() {
            return website;
        }

        public void setWebsite(Object website) {
            this.website = website;
        }

        public Object getNote() {
            return note;
        }

        public void setNote(Object note) {
            this.note = note;
        }

        public String getUse_tag() {
            return use_tag;
        }

        public void setUse_tag(String use_tag) {
            this.use_tag = use_tag;
        }

        public String getSubcription() {
            return subcription;
        }

        public void setSubcription(String subcription) {
            this.subcription = subcription;
        }
    }

    public static class ArticleBean {
        private String id;
        private String content;
        private String musiccode;
        private String adimg;
        private String adlink;
        private String codepic;
        private String auid;
        private String reason;
        private String audit_time;
        private String yrid;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getMusiccode() {
            return musiccode;
        }

        public void setMusiccode(String musiccode) {
            this.musiccode = musiccode;
        }

        public String getAdimg() {
            return adimg;
        }

        public void setAdimg(String adimg) {
            this.adimg = adimg;
        }

        public String getAdlink() {
            return adlink;
        }

        public void setAdlink(String adlink) {
            this.adlink = adlink;
        }

        public String getCodepic() {
            return codepic;
        }

        public void setCodepic(String codepic) {
            this.codepic = codepic;
        }

        public String getAuid() {
            return auid;
        }

        public void setAuid(String auid) {
            this.auid = auid;
        }

        public String getReason() {
            return reason;
        }

        public void setReason(String reason) {
            this.reason = reason;
        }

        public String getAudit_time() {
            return audit_time;
        }

        public void setAudit_time(String audit_time) {
            this.audit_time = audit_time;
        }

        public String getYrid() {
            return yrid;
        }

        public void setYrid(String yrid) {
            this.yrid = yrid;
        }
    }

    public static class CategoryBean {
        private String id;
        private String name;
        private String title;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }
    }

    public static class StatisticsBean {
        private String view_count;
        private String comment_count;

        public String getView_count() {
            return view_count;
        }

        public void setView_count(String view_count) {
            this.view_count = view_count;
        }

        public String getComment_count() {
            return comment_count;
        }

        public void setComment_count(String comment_count) {
            this.comment_count = comment_count;
        }
    }

    public static class RecommendBean {
        private String id;
        private String uid;
        private String title;
        private String description;
        private String cover_url;
        private String track_id;
        private String view_count;
        private String play_count;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCover_url() {
            return cover_url;
        }

        public void setCover_url(String cover_url) {
            this.cover_url = cover_url;
        }

        public String getTrack_id() {
            return track_id;
        }

        public void setTrack_id(String track_id) {
            this.track_id = track_id;
        }

        public String getView_count() {
            return view_count;
        }

        public void setView_count(String view_count) {
            this.view_count = view_count;
        }

        public String getPlay_count() {
            return play_count;
        }

        public void setPlay_count(String play_count) {
            this.play_count = play_count;
        }
    }
}
